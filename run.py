from webserver import server_run
def openBrowser():
  """تابعی برای بازکردن مرورگر وب را به منظور اجرای پروژه"""
  import webbrowser
  webbrowser.open('https://127.0.0.1:5001')
  
if __name__ == "__main__":
  import webbrowser,threading
  th1 = threading.Thread(target=server_run.main)
  th2 = threading.Thread(target=openBrowser)
  th1.start()
  th2.start()

