document.querySelector("ul.child_menu li a").onclick =function(event){
	event.preventDefault()
	let fileName = this.href
	getHTML("taPanel",fileName).then(
		function(data) {
			document.querySelector("#pageContent").innerHTML = data;
			document.querySelector("#pageContent").querySelectorAll("form[validate]").forEach( ( element ) => {
				element.validator = new FormValidator(undefined, element);
			});
		}
	)
}