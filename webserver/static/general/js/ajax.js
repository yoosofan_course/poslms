class AjaxRequest {
    constructor( url, method = "GET", params = {}, xsrf = undefined, async = true ) {
        this.url = url;
        this.method = method;
        this.params = params;
        if(xsrf == undefined)this.xsrf = getCookie("_xsrf");
        else this.xsrf = xsrf;
        this.async = true;
        this.response = "";
        this.responseStatus = 0;
        this.xhrEvent = undefined;
    }
    process() {
        if ( this.method === "GET" ) {
            return this.get();
        }
        
        return this.post();
        
    }
    get() {
        let xhr = new XMLHttpRequest();

        xhr.open( "GET", this.url, this.async );
        if ( this.xsrf !== undefined ) {
            xhr.setRequestHeader("X-Xsrftoken", this.xsrf);
        }
        return new Promise( ( resolve, reject ) => {
            xhr.onload = ( e ) => {
                if ( xhr.readyState === 4 ) {
                    this.responseStatus = xhr.status;
                    if ( xhr.status === 200 ) {
                        this.response = xhr.responseText;
                        this.xhrEvent = e;
                        resolve( this.response );
                    }
                    
                    this.response = xhr.statusText;
                    this.xhrEvent = e;
                    reject( false );
                }
            };
            xhr.onerror = ( e ) => {
                this.response = xhr.statusText;
                this.xhrEvent = e;
                reject( false );
            };
            xhr.send();
        });
        
    }
    post() {
        let xhr = new XMLHttpRequest();

        xhr.open( "POST", this.url, this.async );
        if ( this.xsrf !== undefined ) {
            xhr.setRequestHeader("X-Xsrftoken", this.xsrf);
        }
        return new Promise( ( resolve, reject ) => {
            xhr.onload = ( e ) => {
                if ( xhr.readyState === 4 ) {
                    this.responseStatus = xhr.status;
                    if ( xhr.status === 200 ) {
                        this.response = xhr.responseText;
                        this.xhrEvent = e;
                        resolve( this.response );
                    }
                    
                    this.response = xhr.statusText;
                    this.xhrEvent = e;
                    reject( false );
                }
            };
            xhr.onerror = ( e ) => {
                this.response = xhr.statusText;
                this.xhrEvent = e;
                reject( false );
            };
            xhr.send( this.params );
        });
    }
}