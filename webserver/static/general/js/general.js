function getCookie(name) {
    let cookie = {};
    document.cookie.split(';').forEach(function(el) {
        let [k,v] = el.split('=');
        cookie[k.trim()] = v;
    })
    return cookie[name];
}

var getHTML = async function(panel, fileName){
	let htmlAddress = "/" + panel + "/htmlHandler/" + fileName
    let request = new AjaxRequest(htmlAddress);
    requestPromise = request.process();
	requestPromise.then(function( result ) {
		return result
	})
	requestPromise.catch(function( result ) {
		return "مشکل در بارگذاری صفحه"
	})
	return requestPromise
}

const generalFormSubmit = function(formSelector, apiUrl , callBack = undefined){
	if ( document.querySelector(formSelector).hasAttribute("validate") ) {
		let isValid = document.querySelector(formSelector).validator.isValid()
		if ( isValid === false ) {
			return false;
        }
	}
    let formData = serializeFormData(formSelector)
    formData = {
        'data' : formData
    }
	formData = JSON.stringify(formData)
    postFormData(formData,apiUrl)
    if(callBack != undefined){
        callBack()
    }
}

const generalFormSubmitWithFile = function(formSelector, apiUrl){
	if ( document.querySelector(formSelector).hasAttribute("validate") ) {
		let isValid = document.querySelector(formSelector).validator.isValid()
		if ( isValid === false ) {
			return false;
		}
	}
    let formData = new FormData(document.querySelector(formSelector))
	postFormData(formData,apiUrl)
}

const serializeFormData = function(formSelector){
    //https://stackoverflow.com/a/45899343
    let form = document.querySelector(formSelector)
    let formEntries = new FormData(form).entries();
    let json = Object.assign(...Array.from(formEntries, ([x,y]) => ({[x]:y})));
    return json
}

const postFormData = function(formData, url, errorDivSelector="#error_div"){
    let errorDiv = document.querySelector(errorDivSelector);
    errorDiv.className = "";
    errorDiv.style.display = "none";
    let xsrf = document.querySelector("input[name=_xsrf]").value;
    request = new AjaxRequest(url, "POST", formData, xsrf);
    requestPromise = request.process();
    requestPromise.then(function(result) {
        let getData = JSON.parse(result);
		if(getData['status'] == 200) {
            /*var classList = document.querySelector("#icon-place").classList;
            while (classList.length > 0) {classList.remove(classList.item(0));}
            document.querySelector("#icon-place").classList.add("true");*/
            errorDiv.className = "alert alert-success";
            errorDiv.innerText = getData["message"];
            errorDiv.style.display = "";
		}
		else {
            /*var classList = document.querySelector("#icon-place").classList;
            while (classList.length > 0) {classList.remove(classList.item(0));}
            document.querySelector("#icon-place").classList.add("false");*/
            errorDiv.className = "alert alert-danger";
            errorDiv.innerText = getData["message"];
            errorDiv.style.display = "";
		}
    });
    requestPromise.catch(function(result) {
        /*var classList = document.querySelector("#icon-place").classList;
        while (classList.length > 0) {classList.remove(classList.item(0));}
        document.querySelector("#icon-place").classList.add("false");*/
        errorDiv.className = "alert alert-danger";
        errorDiv.innerText = "خطا در سرور";
        errorDiv.style.display = "";
    });
}