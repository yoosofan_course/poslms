class FormValidator {
    constructor( formSelector = "form", form = undefined ) {
		if ( form === undefined ) {
			this.form = document.querySelector(formSelector);
		}
		else {
			this.form = form;
		}
		this.attribs = [
			"required",
			"range",
			"min",
			"max",
			"length",
			"minlength",
			"maxlength",
			"pattern",
		];
		this.types = [
			"email",
			"url",
			"float",
			"integer",
			"digits",
			"alphanum"
		];
		this.typesRegex = {
			"email" : new RegExp("^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$","i"),
			"url" : new RegExp(`^(?!mailto:)(?:(?:http|https|ftp)://)(?:\\S+(?::\\S*)?@)?(?:(?:(?:[1-9]\\d?|1\\d\\d|2[01]\\d|22[0-3])(?:\\.(?:1?\\d{1,2}|2[0-4]\\d|25[0-5])){2}(?:\\.(?:[0-9]\\d?|1\\d\\d|2[0-4]\\d|25[0-4]))|(?:(?:[a-z\\u00a1-\\uffff0-9]+-?)*[a-z\\u00a1-\\uffff0-9]+)(?:\\.(?:[a-z\\u00a1-\\uffff0-9]+-?)*[a-z\\u00a1-\\uffff0-9]+)*(?:\\.(?:[a-z\\u00a1-\\uffff]{2,})))|localhost)(?::\\d{2,5})?(?:(/|\\?|#)[^\\s]*)?$`, "i"),
			"float" : /^(\+|\-)?\d+(\.\d+)?$/,
			"integer" : /^(\+|\-)?\d+$/,
			"digits" : /^\d$/,
			"alphanum" : /^[a-z]+$/i
		}
		this.messages = {
			type: {
				email:        "این مقدار باید یک ایمیل معتبر باشد",
				url:          "این مقدار باید یک آدرس معتبر باشد",
				float:       "این مقدار باید یک عدد معتبر باشد",
				integer:      "این مقدار باید یک عدد صحیح معتبر باشد",
				digits:       "این مقدار باید یک عدد باشد",
				alphanum:     "این مقدار باید حروف الفبا باشد"
			},
			required:       "این مقدار باید وارد شود",
			pattern:        "این مقدار به نظر می رسد نامعتبر است",
			min:            "این مقدیر باید بزرگتر با مساوی %s باشد",
			max:            "این مقدار باید کمتر و یا مساوی %s باشد",
			range:          "این مقدار باید بین %s و %s باشد",
			length:         "این مقدار نامعتبر است و باید بین %s و %s باشد",
			minlength:      "این مقدار بیش از حد کوتاه است. باید %s کاراکتر یا بیشتر باشد.",
			maxlength:      "این مقدار بیش از حد طولانی است. باید %s کاراکتر یا کمتر باشد.",
		}
		this.inputs = {}
		this.currentElement;
		this.form.querySelectorAll("textarea,input,select").forEach( ( element, index ) => {
			element.insertAdjacentHTML('afterend', '<div id="validator-error-'+index.toString()+'" class="validator-error" style="display:none;"></div>');
			this.inputs[index] = {}
			this.inputs[index]['attributes'] = this.getElementCheckInputAttrs(element)
			this.inputs[index]['type'] = this.getElementType(element)
			this.inputs[index]['element'] = element;
			element.oninput = ( event ) => {
				this.inputValidate(event.target,index);
			}
		});
	}
	isValid() {
		let flag=true;
		for ( let i of Object.keys(this.inputs) ) {
			this.currentElement = this.inputs[i]['element'];
			let idd = "#validator-error-"+i.toString();
			let elementErrorDiv = (this.form).querySelector(idd);
			elementErrorDiv.style.display = "none";
			let result;
			for ( let attribute of this.inputs[i]['attributes'] )  {
				result = this.attributeValidate(attribute);
				if ( result['flag'] === false ){
					elementErrorDiv.style.display = "block";
					elementErrorDiv.innerText = result['error'];
					flag = false;
				}
			}
			let typeValidateResult = this.typeValidate( this.inputs[i]['type'] );
			if ( typeValidateResult['flag'] === false ){
				elementErrorDiv.style.display = "block"
				elementErrorDiv.innerText = typeValidateResult['error']
				flag = false;
			}
		}
		return flag;
	}
	inputValidate( element, i ){
		let idd = "#validator-error-"+i.toString();
		let elementErrorDiv = (this.form).querySelector(idd);
		elementErrorDiv.style.display = "none";
		this.currentElement = element;
		let result;
		for ( let attribute of this.inputs[i]['attributes'] ) {
			result = this.attributeValidate(attribute);
			if ( result['flag'] === false ){
				elementErrorDiv.style.display = "block";
				elementErrorDiv.innerText = result['error'];
				return;
			}
		}
		let typeValidateResult = this.typeValidate( this.inputs[i]['type'] )
		if ( typeValidateResult['flag'] === false ){
			elementErrorDiv.style.display = "block"
			elementErrorDiv.innerText = typeValidateResult['error']
			return;
		}
	}
	attributeValidate ( attribute ) {
		let result = {}
		result['flag'] = true
		switch(attribute['name']) {
			case 'required':
				result = this.validateRequired(attribute);
				break;
			case 'range':
				result = this.validateRange(attribute);
				break;
			case 'min':
				result = this.validateMin(attribute);
				break;
			case 'max':
				result = this.validateMax(attribute);
				break;
			case 'length':
				result = this.validateLength(attribute);
				break;
			case 'minlength':
				result = this.validateMinLength(attribute);
				break;
			case 'maxlength':
				result = this.validateMaxLength(attribute);
				break;
			case 'pattern':
				result = this.validatePattern(attribute);
				break;
		}
		return result
	}
	validateRequired ( attribute ) {
		let result = {}
		if( this.currentElement.value === "" ) {
			result['flag'] = false
			result['error'] = this.messages[attribute['name']]
			return result
		}
		result['flag'] = true
		return result
	}
	validateRange ( attribute ) {
		let result = {}
		if ( /^((\+|\-)?\d+(\.\d+)?)?$/.test(this.currentElement.value) === false ) {
			result['flag'] = false
			result['error'] = this.messages["type"]["number"]
			return result
		}
		let range = attribute['value'].match(/\d+(\.\d+)?/g)
		let min = parseFloat(range[0])
		let max = parseFloat(range[1])
		let inputValue = parseFloat(this.currentElement.value === "" ? max : this.currentElement.value)
		if( min <= inputValue && max >= inputValue ) {
			result['flag'] = true
			return result
		}
		result['flag'] = false
		let minMax = [min, max]
		result['error']  = this.messages[attribute['name']].replace(/%s/g,function() {
			return minMax.shift();
		});
		return result
	}
	validateMin ( attribute ) {
		let result = {}
		if ( /^((\+|\-)?\d+(\.\d+)?)?$/.test(this.currentElement.value) === false ) {
			result['flag'] = false
			result['error'] = this.messages["type"]["number"]
			return result
		}
		let min = parseFloat(attribute['value'])
		let inputValue = parseFloat(this.currentElement.value === "" ? min : this.currentElement.value)
		if( min <= inputValue ) {
			result['flag'] = true
			return result
		}
		result['flag'] = false
		result['error']  = this.messages[attribute['name']].replace("%s", [min])
		return result
	}
	validateMax ( attribute ) {
		let result = {}
		if ( /^((\+|\-)?\d+(\.\d+)?)?$/g.test(this.currentElement.value) === false ) {
			result['flag'] = false
			result['error'] = this.messages["type"]["number"]
			return result
		}
		let max = parseFloat(attribute['value'])
		let inputValue = parseFloat(this.currentElement.value === "" ? max : this.currentElement.value)
		if( max >= inputValue ) {
			result['flag'] = true
			return result
		}
		result['flag'] = false
		result['error']  = this.messages[attribute['name']].replace("%s", [max])
		return result
	}
	validateLength ( attribute ) {
		let result = {}
		let range = attribute['value'].match(/\d+/g)
		let inputValue = this.currentElement.value
		let min = parseInt(range[0])
		let max = parseInt(range[1])
		if( min <= inputValue.length && max >= inputValue.length || inputValue.length === 0 ) {
			result['flag'] = true
			return result
		}
		result['flag'] = false
		let minMax = [min, max]
		result['error']  = this.messages[attribute['name']].replace(/%s/g,function() {
			return minMax.shift();
		});
		return result
	}
	validateMinLength ( attribute ) {
		let result = {}
		let inputValue = this.currentElement.value
		let min = parseInt(attribute['value'])
		if( min <= inputValue.length || inputValue.length === 0 ) {
			result['flag'] = true
			return result
		}
		result['flag'] = false
		result['error']  = this.messages[attribute['name']].replace("%s", [min])
		return result
	}
	validateMaxLength ( attribute ) {
		let result = {}
		let max = parseInt(attribute['value'])
		let inputValue = this.currentElement.value
		if( max >= inputValue.length || inputValue.length === 0 ) {
			result['flag'] = true
			return result
		}
		result['flag'] = false
		result['error']  = this.messages[attribute['name']].replace("%s", [max])
		return result
	}
	validatePattern ( attribute ) {
		let result = {}
		let regexString = attribute['value']
		let flags = regexString.replace(/.*\/([gimy]*)$/, '$1');
		let pattern = regexString.replace(new RegExp('^/(.*?)/'+flags+'$'), '$1');
		let regex = new RegExp(pattern, flags);
		if ( regex.test(this.currentElement.value) === false ) {
			result['flag'] = false
			result['error'] = this.messages[attribute['name']]
			return result
		}
		result['flag'] = true
		return result
	}
	typeValidate ( type ){
		let result = {}
		result['flag'] = true
		if ( this.types.includes(type) === false ) {
			return result
		}
		if ( this.typesRegex[type].test(this.currentElement.value) === false && this.currentElement.value !== "" ) {
			result['flag'] = false
			result['error'] = this.messages['type'][type]
			return result
		}
		return result
	}
	getElementCheckInputAttrs ( element ) {
		let attrsAndValue = []
		let elementAttrs = this.getElementAllAttrs(element);
		this.attribs.forEach( ( attribute ) => {
			if ( elementAttrs.includes(attribute) ){
				attrsAndValue.push({
					'name' : attribute,
					'value' : element.getAttribute(attribute)
				})
			}
		})
		return attrsAndValue
	}
	getElementAllAttrs ( element ) {
		return Object.keys(element.attributes).map( (index) => {
			return element.attributes[index].name;
		});
	}
	getElementType( element ) {
		let elementType = element.getAttribute("type")
		if ( elementType == null ) {
			elementType = ""
		}
		return elementType
	}
}