
function bringalluserbroadcast(){

    let xsrf = document.querySelector("input[name=_xsrf]").value;
    request = new AjaxRequest('/api/broadcast/showall', "GET", '', xsrf);
    requestPromise = request.process();
    errorDiv = document.getElementById("error_div")
    requestPromise.then(function(result) {
        let getData = JSON.parse(result);
        if(getData['status'] == 200) {
            html = ''
            for (let i = 0 ; i < getData['data'].length ; i++){
                html += ` <tr>
                            <th scope="row">`+i+`</th>
                            <td id="title`+getData['data'][i][0]+`">`+getData['data'][i][1]+`</td>
                            <td id="desc`+getData['data'][i][0]+`">`+getData['data'][i][2]+`</td>
                            <td>`+getData['data'][i][4]+`</td>
                            <td><button onclick="loadedit(`+getData['data'][i][0]+`)" type="button" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-sm">ویرایش</button></td>
                            <td><a onclick='deleteBR(`+getData['data'][i][0]+`)'>حذف</a></td>
                            
                        </tr>`
                document.querySelector("#tabledata").innerHTML = html
            }
        }
        else {
            errorDiv.className = "alert alert-danger";
            errorDiv.innerText = getData["message"];
        }
    });
    requestPromise.catch(function(result) {
        errorDiv.className = "alert alert-danger";
        errorDiv.innerText = "خطا در سرور";
    });
    
}
function loadedit(id){
    document.getElementById('modalx').style.display = 'block'
    document.querySelector("#id").value = id
    let title = document.querySelector("#title"+id).innerHTML
    let desc = document.querySelector("#desc"+id).innerHTML
    document.querySelector("#title").value = title
    document.querySelector("#description").value = desc
}
function closeit(){
    document.getElementById('modalx').style.display = 'none'
}
function handler_editbroadcast(){
    let formData = new FormData(document.getElementById("broadcast"))
    let xsrf = document.querySelector("input[name=_xsrf]").value;
    request = new AjaxRequest('/api/broadcast/edit', "POST",formData, xsrf);
    errorDiv = document.getElementById("error_div")
    requestPromise = request.process();
    requestPromise.then(function(result) {
        let getData = JSON.parse(result);
        if(getData['status'] == 200) {
            bringalluserbroadcast()
            document.getElementById("closemodal").click();
        }
        else {
            errorDiv.className = "alert alert-danger";
            errorDiv.innerText = getData["message"];
        }
    });
    requestPromise.catch(function(result) {
        errorDiv.className = "alert alert-danger";
        errorDiv.innerText = "خطا در سرور";
    });
}
function deleteBR(id){
    let xsrf = document.querySelector("input[name=_xsrf]").value;
    request = new AjaxRequest('/api/broadcast/del/'+id, "GET" ,'', xsrf);
    errorDiv = document.getElementById("error_div")
    requestPromise = request.process();
    requestPromise.then(function(result) {
        let getData = JSON.parse(result);
        if(getData['status'] == 200) {
            bringalluserbroadcast()
        }
        else {
            errorDiv.className = "alert alert-danger";
            errorDiv.innerText = getData["message"];
        }
    });
    requestPromise.catch(function(result) {
        errorDiv.className = "alert alert-danger";
        errorDiv.innerText = "خطا در سرور";
    });
}


