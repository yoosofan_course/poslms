function handler_professor(){
    let formData = new FormData(document.getElementById("professor"))
    
    $.ajax({
        type: "POST",
        url: '/api/professor/add',
        data: formData,
        processData: false,
        contentType: false,  
        success: function (data) {
             let parsedData = JSON.parse(data);
             if(parsedData['status'] == 200){
                 alert(parsedData['message']);
                 $("#professor")[0].reset();
             }else{
                alert(parsedData['message']);

             }
            return false;
        }
    });
}
