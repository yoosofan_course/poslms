window.onload = function() {
	setTimeout(function(){ document.querySelector("#cont .demo").classList.add("animation"); }, 200);

	document.querySelector("#login_username").onfocus = function(){
	  var classList = document.querySelector("#icon-place").classList;
	  while (classList.length > 0) {classList.remove(classList.item(0));}
	  document.querySelector("#icon-place").classList.add("user");
	}
	document.querySelector("#login_password").onfocus = function(){
	  var classList = document.querySelector("#icon-place").classList;
	  while (classList.length > 0) {classList.remove(classList.item(0));}
	  document.querySelector("#icon-place").classList.add("pass");
	}
	document.querySelector("button.login__submit").onmouseover = function(){
	  var classList = document.querySelector("#icon-place").classList;
	  while (classList.length > 0) {classList.remove(classList.item(0));}
	  document.querySelector("#icon-place").classList.add("login");
	}
	document.querySelector("#icon-place").onmouseover = function(){
	  var classList = document.querySelector("#icon-place").classList;
	  while (classList.length > 0) {classList.remove(classList.item(0));}
	}
	document.querySelector("#bullets div.one").onclick = function(){
	  var classList = document.querySelector("#cont").classList;
	  while (classList.length > 0) {classList.remove(classList.item(0));}
	  for (var i = 0; i < document.querySelectorAll("#bullets div").length; i++) {
	  	document.querySelectorAll("#bullets div")[i].classList.remove("active");		  
	  }
	  document.querySelector("#bullets div.one").classList.add("active");
	}
	document.querySelector("#bullets div.two").onclick = function(){
	  var classList = document.querySelector("#cont").classList;
	  while (classList.length > 0) {classList.remove(classList.item(0));}
	  document.querySelector("#cont").classList.add("second-bg");
	  document.querySelector("#bullets").classList.add("white");	
	  for (var i = 0; i < document.querySelectorAll("#bullets div").length; i++) {
	  	document.querySelectorAll("#bullets div")[i].classList.remove("active");		  
	  }
	  document.querySelector("#bullets").classList.remove("white");
	  document.querySelector("#bullets div.two").classList.add("active");
	}
	document.querySelector("#bullets div.three").onclick = function(){
	  var classList = document.querySelector("#cont").classList;
	  while (classList.length > 0) {classList.remove(classList.item(0));}
	  document.querySelector("#cont").classList.add("third-bg");
	  document.querySelector("#bullets").classList.add("white");
	  for (var i = 0; i < document.querySelectorAll("#bullets div").length; i++) {
	  	document.querySelectorAll("#bullets div")[i].classList.remove("active");		  
	  }
	  document.querySelector("#bullets div.three").classList.add("active");
	}



	var animating = false,
        submitPhase1 = 1100,
        submitPhase2 = 400,
        logoutPhase1 = 800,
        login = document.querySelector(".login"),
        app = document.querySelector(".app");

    function ripple(elem, e) {
        document.querySelector(".ripple").parentNode.removeChild(document.querySelector(".ripple"));
        var elTop = elem.offsetTop,
            elLeft = elem.offsetLeft,
            x = e.pageX - elLeft,
            y = e.pageY - elTop;
        var ripple = document.querySelector("<div class='ripple'></div>");
        ripple.style.top = y;
        ripple.style.left = x;
        elem.appendChild(ripple);
    };

	
	document.querySelectorAll(".login__form input").forEach( function( element ){
		element.onkeypress = function(event) {
			if (event.which == 13) {
				event.preventDefault();
				loginFormSubmit()
			}
		};
	})

    document.querySelector(".login__submit").onclick = function() {
		loginFormSubmit()
	};

	const loginFormSubmit = function (){
		document.querySelector(".login__submit").disabled = true;
		document.querySelector(".login__submit").classList.remove("login__success")
		document.querySelector(".login__submit").classList.remove("login__failed")
        /*var that = this;
		ripple(document.querySelector(that), e);*/
		document.querySelector(".login__submit").classList.add("processing");
		validateAndSendData();
	}

	const validateAndSendData = function(){
		let username = document.querySelector("#login_username").value
        let password = document.querySelector("#login_password").value
        let loginSubmit = document.querySelector(".login__submit")
		let xsrf = getCookie("_xsrf")
		if( username !== "" && password !== "" ){
            formData = {
                data: {
                    'username': username,
                    'password': password
                }
            }
            formData = JSON.stringify(formData)
            request = new AjaxRequest("/api/login", "POST", formData, xsrf);
            requestPromise = request.process();
            requestPromise.then(function(result) {
                let data = JSON.parse(result);
                if(data['status'] == 301) {
					document.querySelector(".login__submit").classList.remove("processing")
					document.querySelector(".login__submit").classList.add("login__success")
					document.querySelector(".login__submit").innerText = "در حال انتقال به پنل کاربری";
					window.location = data['data']['redirect']
                }
                else if(data['status'] == 401){
					document.querySelector(".login__submit").classList.remove("processing")
					document.querySelector(".login__submit").classList.add("login__failed")
					document.querySelector(".login__submit").innerText = "نام کاربری یا کلمه عبور صحیح نیست";

				}
                else{
					document.querySelector(".login__submit").classList.remove("processing")
					document.querySelector(".login__submit").classList.add("login__failed")
					document.querySelector(".login__submit").innerText = "خطا در ارتباط";

                }
            });
            requestPromise.catch(function(result) {
				document.querySelector(".login__submit").classList.remove("processing")
				document.querySelector(".login__submit").classList.add("login__failed")
				document.querySelector(".login__submit").innerText = "خطا در ارتباط";
		   });
		}
		else{
			document.querySelector(".login__submit").classList.remove("processing")
			document.querySelector(".login__submit").classList.add("login__failed")
			document.querySelector(".login__submit").innerText ="لطفا همه فیلد ها را وارد کنید";
		}
		document.querySelector(".login__submit").disabled = false;
		
	}
};