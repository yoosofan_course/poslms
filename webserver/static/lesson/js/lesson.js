
function handler_lesson(){
    let formData = new FormData(document.getElementById("lesson"))
    
    $.ajax({
        type: "POST",
        url: '/api/lesson/add',
        data: formData,
        processData: false,
        contentType: false,  
        success: function (data) {
             let parsedData = JSON.parse(data);
             if(parsedData['status'] == 200){
                 alert(parsedData['message']);
                 $("#lesson")[0].reset();
             }
            return false;
        }
    });
}


