class Updater{
    constructor(token){
        this.token = token;
        this.reconnct()
        let _this = this;
        this.socket.onmessage = function(event) {
            _this.showMessage(event.data);
        }
 
    }

    reconnct(){
        var url = "wss://" + location.host + "/chatsocket";
        this.socket = new WebSocket(url);
    }

    showMessage(message) {
       let data = JSON.parse(message);
       console.log(data['type'],message)
       if(data['type'] == 'init'){
           this.chat.init_users(data['data'])
       }else if(data['type'] == 'socket-start'){
            this.chat = new Chat(this);
       }else if(data['type'] == 'message_sent'){
            this.chat.message_was_sent(data)
        console.log(data)
       }else if(data['type'] == 'receive'){
        this.chat.message_received(data);
        console.log(data)
       }else if(data['type'] == 'peer_messages'){
           this.chat.load_message_for_peer(data)
       }else if(data['type'] == 'init_new_user'){
            if(data['status'] == false){
                alert('کاربر ' + data['user'] + 'وجود ندارد' );
            }else{
                let new_user = this.chat.addUser(data['user'])
                new_user.select_for_chat()
            }
        
       }
    }

    send_request(request){
        if(this.socket === undefined) this.reconnct();
        this.socket.send(JSON.stringify({'request':request, 'token':this.token}));
    }
}
 
class Chat{
    constructor(updater, users = []){
        this.users = [];
        updater.send_request({'type':'init'})
        this.updater = updater;
        this.selected_user = ''
        let _this = this;
        let btn_send = document.getElementById("btn_send")
        btn_send.onclick = function(){
            if(_this.selected_user != '' ){
                _this.send_message();
            }
            var chatEl = document.getElementById("message-bar");
            chatEl.scrollTop = chatEl.scrollHeight;
        }
        document.getElementById("new-contact").onclick = function(){
            let user = document.getElementById("new_contact_input");
            user = user.value;
            for (var i = 0; i < document.querySelectorAll("#contact li").length; i++) {
                if(document.querySelectorAll("#contact li div p")[i].innerHTML == user) {
                    return 0
                }
            }
            _this.updater.send_request({
                        'type': 'message_to_new_user',
                        'user': user,
                    });

        }

    }

    init_users(users){
        for(const user of users){
            this.users.push(new User(user[0], this))
        }
    }

    send_message(){
        let message = document.getElementById("text-message").value;
        if(message != ''){
            this.updater.send_request({
                'type': 'send_message',
                'text': message,
                'to':this.selected_user
            });
        }
        document.getElementById("text-message").value = "";
        setTimeout(function(){        var chatEl = document.getElementById("message-bar");
        chatEl.scrollTop = chatEl.scrollHeight;}, 50);
    }

    message_was_sent(message){
        for(const user of this.users){
            console.log(user, message["to"])
            if(user.user == message["to"]){
                user.add_message(message, "sender");
                user.load();
                return 0;
            }
        }
        let new_user = this.addUser(message["to"]);
        new_user.add_message(message, "sender");
        new_user.load();
    }

    message_received(message){
        for(const user of this.users){
            console.log(user, message["to"])
            if(user.user == message["sender"]){
                user.add_message(message, "receiver");
                user.load();
                return 0;
            }
        }
        let new_user = this.addUser(message["sender"]);
        new_user.add_message(message, "receiver");
        new_user.load();
        
    }

    load_message_for_peer(message){
        for(const user of this.users){
            if(user.user == message["peer"]){
                user.load_messages(message['data']);
                user.load();
                return 0;
            }
        }        
        
    }
    
    addUser(user){
        let new_user = new User(user, this);
        this.users.push(new_user);
        return new_user;
    }

}

class User{
    constructor(user, chat){
        this.user = user
        this.messages = []
        this.socket = chat.updater.socket
        this.pending = []
        this.chat = chat
        let template = document.createElement("li")
        template.innerHTML = ChatTemlate.generate_contact_template(user)
        let _this = this;
        template.onclick = function(){
            for(var i=0;i<document.querySelectorAll("#contact li").length;i++) {
                document.querySelectorAll("#contact li")[i].classList.remove("active-contact");
            }
            template.classList.add("active-contact")
            _this.chat.selected_user = _this.user
            _this.ask_for_messages();
    setTimeout(function(){        var chatEl = document.getElementById("message-bar");
        chatEl.scrollTop = chatEl.scrollHeight;}, 50);
        }
        document.getElementById("contact").appendChild(template)
    }

    select_for_chat(){
        this.chat.selected_user = this.user
    }

    add_message(message, type="receiver"){
        this.messages.push({"message":message["message"], "type":type, "id":message["id"]})

    }

    load_messages(messages){
        for(const message of messages){
            if(message[1] == this.user){ //current session is receiver
                this.messages.push({"message":message[3], "type":"receiver", "id":message[0]})

            }else{//current session is sender
                this.messages.push({"message":message[3], "type":"sender", "id":message[0]})

            }
        }
    }
    
    ask_for_messages(){
        this.chat.updater.send_request({"type":"peer_messages", "peer":this.user, "limit":10, "offset":0})
    }

    load(){
        document.getElementById("message-bar").innerHTML = "";
        for(const message of this.messages){
            this.load_message(message);
        }        
    }

    load_message(message){
        let new_message = document.createElement("p");
        new_message.innerText = message["message"];
        new_message.setAttribute("class", message["type"] + " message ");
        new_message.setAttribute("id", "msg-"+message["id"]);
        document.getElementById("message-bar").appendChild(new_message);

    }

    load_this_user(){

    }


}

class ChatTemlate{
    static generate_contact_template(username){
        return `
            <div>
                <p>${username}</p>
            </div>  
        `   
    }
}
