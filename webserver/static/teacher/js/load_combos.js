function showCourseExercises() {
    let formData = serializeFormData("#get_course_member")
    formData = {
        'data' : formData
    }
    formData = JSON.stringify(formData)
    let xsrf = document.querySelector("input[name=_xsrf]").value;
    request = new AjaxRequest("/api/course/exercises", "POST", formData, xsrf);
    requestPromise = request.process();
    requestPromise.then(function(result) {
        let getData = JSON.parse(result);
        if(getData['status'] == 200) {
            const usersTableDiv = document.querySelector("#users_table_div");
            const usersTableBody = usersTableDiv.querySelector("tbody");
            usersTableDiv.style.display = "";
            usersTableBody.innerHTML = "";
            const data = getData['data'];
            for ( const item of data ) {
                console.log(item);
                usersTableBody.innerHTML += `
                    <tr>
                        <th scope="row">${item[0]}</th>
                        <td>${item[1]}</td>
                        <td>${item[2]}</td>
                        <td><a href="/media/exercises/${item[3]}">دانلود</a></td>
                        <td>${item[4]}</td>
                        <td>${item[5]}</td>
                        <td><button class="btn btn-danger" onclick="return deleteExerciseFromCourse(this,'${item[0]}')">حذف</button></td>
                    </tr>
                `
            }
        }
    });
}

function showCourseMember() {
	let formData = serializeFormData("#get_course_member")
	formData = {
		'data' : formData
	}
	formData = JSON.stringify(formData)
	let xsrf = document.querySelector("input[name=_xsrf]").value;
	request = new AjaxRequest("/api/member/getcoursemember", "POST", formData, xsrf);
	requestPromise = request.process();
	requestPromise.then(function(result) {
		let getData = JSON.parse(result);
		if(getData['status'] == 200) {
			const usersTableDiv = document.querySelector("#users_table_div");
			const usersTableBody = usersTableDiv.querySelector("tbody");
			usersTableDiv.style.display = "";
			usersTableBody.innerHTML = "";
			const data = getData['data'];
			for ( i in data ) {
				usersTableBody.innerHTML += `
					<tr>
						<th scope="row">${(parseInt(i)+1).toString()}</th>
						<td>${data[i][0]}</td>
						<td>${data[i][1]}</td>
						<td>${data[i][2]}</td>
						<td><button class="btn btn-danger" onclick="return deleteUserFromCourse(this,'${data[i][2]}')">حذف</button></td>
					</tr>
				`
			}
		}
	});
}


function showCourseExercisesStudent() {
    let formData = serializeFormData("#get_course_member")
    formData = {
        'data' : formData
    }
    formData = JSON.stringify(formData)
    let xsrf = document.querySelector("input[name=_xsrf]").value;
    request = new AjaxRequest("/api/exercise/courseExercise", "POST", formData, xsrf);
    requestPromise = request.process();
    requestPromise.then(function(result) {
        let getData = JSON.parse(result);
        if(getData['status'] == 200) {
            const usersTableDiv = document.querySelector("#users_table_div");
            const usersTableBody = usersTableDiv.querySelector("tbody");
            usersTableDiv.style.display = "";
            usersTableBody.innerHTML = "";
            console.log(getData)
            
            const data = getData['data'];
            for ( const item of data ) {
                console.log(item);
                usersTableBody.innerHTML += `
                    <form>
                    <tr>
                        
                            <td scope="row">${item[0]}</td>
                            <td>${item[1]}</td>
                            <td>${item[2]}</td>
                            <td><a href="/media/exercises/${item[3]}">دانلود</a></td>
                            <td>${item[4]}</td>
                            <td>${item[5]}</td>
                            <td><input id="title-` + item[0] + `" /></td>
                            <td><input id="description-` + item[0] + `" /></td>
                            <td><input id="file-` + item[0] + `" type="file"/></td>
                            <td><button class="btn btn-danger" onclick="return uploadExercise('${item[0]}')">آپلود</button></td>
                            <td><div id="error-` + item[0] + `"></div></td>
                    </tr>
                    </form>
                    `

                }
        }
    });
}

function ShowCourseExercisesMarks() {
    let formData = serializeFormData("#get_course_member")
    formData = {
        'data' : formData
    }
    formData = JSON.stringify(formData)
    let xsrf = document.querySelector("input[name=_xsrf]").value;
    request = new AjaxRequest("/api/exercise_answer/marks", "POST", formData, xsrf);
    requestPromise = request.process();
    requestPromise.then(function(result) {
        let getData = JSON.parse(result);
        if(getData['status'] == 200) {
            const usersTableDiv = document.querySelector("#users_table_div");
            const usersTableBody = usersTableDiv.querySelector("tbody");
            usersTableDiv.style.display = "";
            usersTableBody.innerHTML = "";
            const data = getData['data'];
            for ( const item of data ) {
                console.log(item);
                usersTableBody.innerHTML += `
                    <tr>
                            <td scope="row">${item[0]}</td>
                            <td>${item[10]}</td>
                            <td>${item[3]}</td>
                            <td>${item[4]}</td>
                            <td><a href="/media/exercises_answers/${item[5]}">دانلود</a></td>
                            <td>${item[6]}</td>

                    </tr>
                    `

                }
        }
    });
}


function uploadExercise(id){
    let title = document.getElementById("title-"+id);
    let description = document.getElementById("description-"+id);
    let file = document.getElementById("file-"+id);
    let data = new FormData();
    data.append("title", title.value);
    data.append("description", description.value);
    data.append("file", file.files[0]);
    data.append("id", id);
	let xsrf = getCookie("_xsrf");
	let request = new AjaxRequest("/api/exercise_answer/add", "POST", data, xsrf);
	requestPromise = request.process();
	requestPromise.then(function(result) {
		let getData = JSON.parse(result);
        if(getData['status'] == 200) {
            alert(getData['message']);
            title.value = "";
            description.value = "";
            file.value = "";
            
        }else{
            alert(getData['message']);
        }
	});
    

}
