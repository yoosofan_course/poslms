function generate_link_for_course(id){
    let form = new FormData()
    form.append("id", id)
    request = new AjaxRequest("/api/course/link_generator", "POST", form);
    requestPromise = request.process();
    requestPromise.then(function(result) {
        let getData = JSON.parse(result);
        console.log(getData);
		if(getData['status'] == 200) {
            alert(getData['link']);
        }
		else {
            alert('شما دسترسی ندارید یا خطایی رخ داده است')
        }
    });
    requestPromise.catch(function(result) {
        alert('شما دسترسی ندارید یا خطایی رخ داده است')
    });



}

function revoke_link_for_course(id){
    let form = new FormData()
    form.append("id", id)
    request = new AjaxRequest("/api/course/link_revoker", "POST", form);
    requestPromise = request.process();
    requestPromise.then(function(result) {
        let getData = JSON.parse(result);
		if(getData['status'] == 200) {
            alert(getData['message']);
        }
		else {
            alert('شما دسترسی ندارید یا خطایی رخ داده است')
        }
    });
    requestPromise.catch(function(result) {
        alert('شما دسترسی ندارید یا خطایی رخ داده است')
    });

}