
function deleteUserFromCourse(that,username){
	let formData = serializeFormData("#get_course_member")
	formData['member_user'] = username
	formData = {
		'data' : formData
	}
	formData = JSON.stringify(formData)
	let xsrf = document.querySelector("input[name=_xsrf]").value;
	let request = new AjaxRequest("/api/member/del", "POST", formData, xsrf);
	requestPromise = request.process();
	requestPromise.then(function(result) {
		let getData = JSON.parse(result);
		if(getData['status'] == 200) {
			const usersTableBody = document.querySelector("#users_table_div").querySelector("tbody");
			usersTableBody.removeChild(that.parentElement.parentElement)
		}
	});
}

function deleteExerciseFromCourse(that, exercise){
	let formData = serializeFormData("#get_course_member")
	formData['exercise'] = exercise
	formData = {
		'data' : formData
	}
	formData = JSON.stringify(formData)
	let xsrf = document.querySelector("input[name=_xsrf]").value;
	let request = new AjaxRequest("/api/exercise/del", "POST", formData, xsrf);
	requestPromise = request.process();
	requestPromise.then(function(result) {
		let getData = JSON.parse(result);
		if(getData['status'] == 200) {
			const usersTableBody = document.querySelector("#users_table_div").querySelector("tbody");
			usersTableBody.removeChild(that.parentElement.parentElement)
		}
	});
}