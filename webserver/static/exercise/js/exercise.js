function handler_exercise(){
    let formData = new FormData(document.getElementById("exercise"))

    let file = document.getElementById('file')
    let title = document.getElementById('title')
    let description = document.getElementById('description')
        
}
function ExerciseUsers(id){
    let errorDiv = document.getElementById("error_div")
    let xsrf = document.querySelector("input[name=_xsrf]").value;
    request = new AjaxRequest('/api/exercise/exerciseUsers/'+id, "GET", '', xsrf);
    requestPromise = request.process();
    requestPromise.then(function(result) {
        let getData = JSON.parse(result);
        if(getData['status'] == 200) {
            html =` <thead>
            <tr>
                <th>#</th>
                <th>شماره دانشجو</th>
                <th>فایل پاسخ</th>
                <th>نمره</th>
            </tr>
            </thead>
            <tbody ><form validate id='SubmitScore'>`
            for (let i = 0 ; i < getData['data'].length ; i++){
                
                html += `            
                <tr>
                    <th scope="row">`+i+`</th>
                    <td id="name`+getData['data'][i][2]+`">`+getData['data'][i][2]+`</td>
                    <td id="file`+getData['data'][i][2]+`"><a href='/media/exercises_answers/`+getData['data'][i][5]+`'>فایل تمرین</a></td>
                    <td id="score`+getData['data'][i][2]+`"><input value =`+getData['data'][i][6]+` class='scores' type='number' max=100 min=0 name=`+getData['data'][i][2]+` placeholder=`+getData['data'][i][6]+`></td>
                </tr>
                `
                
            }
            document.querySelector("#tabledata").innerHTML = html + `</form><button onclick='SubmitScore(`+getData['data'][0][0]+`)'>تایید نمره</button></tbody>`
        }
        else {
            errorDiv.className = "alert alert-danger";
            errorDiv.innerText = getData["message"];
        }
    });
    requestPromise.catch(function(result) {
        errorDiv.className = "alert alert-danger";
        errorDiv.innerText = "خطا در سرور";
    });

}
function SubmitScore(id){
    
    let errorDiv = document.getElementById("error_div")
    let xsrf = document.querySelector("input[name=_xsrf]").value;
    var formData = new FormData();
    inputs = document.getElementsByClassName('scores');
    console.log(inputs)
    for (index = 0; index < inputs.length; ++index) {
        formData.append(inputs[index].name,inputs[index].value)
    }
    request = new AjaxRequest('/api/exercise/submitScore/'+id, "POST", formData, xsrf);
    requestPromise = request.process();
    requestPromise.then(function(result) {
        let getData = JSON.parse(result);
        if(getData['status'] == 200) {
            errorDiv.className = "alert alert-success";
            errorDiv.innerText = getData["message"];
        }
        else {
            errorDiv.className = "alert alert-danger";
            errorDiv.innerText = getData["message"];
            errorDiv.style.display = "";
        }
    });
    requestPromise.catch(function(result) {
        errorDiv.className = "alert alert-danger";
        errorDiv.innerText = "خطا در سرور";
        errorDiv.style.display = "";
    });

}
function ShowCE(id){
    let errorDiv = document.getElementById("error_div")
    let xsrf = document.querySelector("input[name=_xsrf]").value;
    request = new AjaxRequest('/api/exercise/showCourseExercise/'+id, "GET", '', xsrf);
    requestPromise = request.process();
    requestPromise.then(function(result) {
        let getData = JSON.parse(result);
        if(getData['status'] == 200) {
            html =` <thead>
            <tr>
                <th>#</th>
                <th>کد تمرین</th>
                <th>نام تمرین</th>
                <th>لیست دانشجویان</th>
            </tr>
            </thead>
            <tbody >`
            for (let i = 0 ; i < getData['data'].length ; i++){
                
                html += `            
                <tr>
                    <th scope="row">`+i+`</th>
                    <td id="code`+getData['data'][i][0]+`">`+getData['data'][i][1]+`</td>
                    <td id="name`+getData['data'][i][0]+`">`+getData['data'][i][3]+`</td>
                    <td><button onclick='ExerciseUsers(`+getData['data'][i][0]+`)'>نمایش</button></td>
                </tr>
                `
                
            }
            document.querySelector("#tabledata").innerHTML = html + `</tbody>`
        }
        else {
            errorDiv.className = "alert alert-danger";
            errorDiv.innerText = getData["message"];
            errorDiv.style.display = "";
        }
    });
    requestPromise.catch(function(result) {
        errorDiv.className = "alert alert-danger";
        errorDiv.innerText = "خطا در سرور";
        errorDiv.style.display = "";
    });

}