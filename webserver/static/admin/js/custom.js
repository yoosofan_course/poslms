const menus_links = document.querySelectorAll("ul.child_menu li a")
for(const link of menus_links){
	link.onclick = function(event){
		event.preventDefault()
		let fileName = this.href
		getHTML("adminPanel",fileName).then(
			function(data) {
				document.queryselector("#pageContent").innerHTML = data;
				document.querySelector("#pageContent").querySelectorAll("form[validate]").forEach( ( element ) => {
					element.validator = new FormValidator(undefined, element);
				});
			}
		)
	}
}