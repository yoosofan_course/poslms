import json

def checkInput(self,postArgs) :

    if type(postArgs) is list :
        try :
            returnData = {}
            flag = False
            for arg in postArgs :

                if arg[0] == "*" :
                    argWithoutStar = arg[1:]
                    flag = True
                else :
                    argWithoutStar = arg
                    flag = False
                
                if argWithoutStar not in self.request.arguments :
                    return False

                currentArg = self.request.arguments[argWithoutStar]
                if flag :
                    if type(currentArg) is list :
                        if currentArg[0].decode("utf-8", "strict") == "" :
                            return False
                    elif type(currentArg) is bytes :
                        if currentArg.decode("utf-8", "strict") == "" :
                            return False
                    else :
                        if str(currentArg) == "" :
                            return False

                if type(currentArg) is list :
                    returnData[argWithoutStar] = currentArg[0].decode("utf-8", "strict")
                elif type(currentArg) is bytes :
                    returnData[argWithoutStar] = currentArg.decode("utf-8", "strict")
                else :
                    returnData[argWithoutStar] = str(currentArg)

            return returnData
        except :
            return False

def checkInputJson(self,postArgs,jsonDataParam="data") :
    try :
        arguments = json.loads((self.request.body).decode("utf-8"))[jsonDataParam]
        returnData = {}
        flag = False
        for arg in postArgs :

            if arg[0] == "*" :
                argWithoutStar = arg[1:]
                flag = True
            else :
                argWithoutStar = arg
                flag = False
            
            if argWithoutStar not in arguments :
                return False

            currentArg = arguments[argWithoutStar]
            if flag :
                if str(currentArg) == "" :
                    return False

            returnData[argWithoutStar] = str(currentArg)

        return returnData
    except Exception as err:
        print(err)
        return False