import momoko
class db:
    def __init__(self, application, ioloop, dbSetting):
        dsn = ''
        for key,val in dbSetting.dbSetting.items():
          dsn += key + '=' + val + ' '
        #dsn = 'dbname=dc user=postgres password=R00m1135 host=127.0.0.1 port=5432'
        self.internal_db = momoko.Pool(dsn=dsn,size=dbSetting.momokoSetting['size'], max_size=dbSetting.momokoSetting['max_size'], ioloop=ioloop, setsession=dbSetting.momokoSetting['setsession'],raise_connect_errors=False)
        # this is a one way to run ioloop in sync
        future = self.internal_db.connect()
        ioloop.add_future(future, lambda f: ioloop.stop())
        ioloop.start()
        future = self.internal_db.register_json()
        # This is the other way to run ioloop in sync
        ioloop.run_sync(lambda: future)
    def __enter__(self): 
        return self;
    def __exit__(self, exc_type, exc_value, exc_traceback):
        if exc_type is not None and exc_type is not KeyboardInterrupt:
            msg= 'exc_type= '  + str(exc_type)+'\n'
            msg+='exc_value= ' + str(exc_value)+'\n'
            print(msg)
            traceback.print_tb(exc_traceback)
        if exc_type is KeyboardInterrupt:return False
        return True
  
    async def transaction(self,arg1):
        """متد تراکنش"""
        res={}; r1='';res['data']=[]
        r1= await self.internal_db.transaction(arg1)

        return res
    async def execute(self,arg1,arg2=None):
        """متد اجرا"""
        res={}; r1=''
        if arg2!=None:     r1=await self.internal_db.execute(arg1,arg2)
        else:              r1=await self.internal_db.execute(arg1)
        res['data']=r1.fetchall()
        return res
    async def commit(self):
        """متد کامیت"""
        await self.internal_db.execute('COMMIT;');
    async def q(self,qs,fetchType='n',arg1=()):
        '''       'n' for fetch none      'a' for fetch all      'o' for fetch one          returns      fetched data      result of instruction    ''' #res['data']=json.JSONEncoder({'flag':False})
        res={};    gxc=False;    res['flag']=False;    res['data']={'flag':False}
        try:
            if arg1==(): r1=await self.internal_db.execute(qs);
            else:        r1=await self.internal_db.execute(qs,arg1)
            if   fetchType == 'a': res['data']= r1.fetchall();res['flag']=True;
            elif fetchType == 'o': res['data']= r1.fetchone();res['flag']=True;
            elif fetchType == 'n': res['data']=''            ;res['flag']=True;
            else:print('Error in "db.db.q" fetchType is incorrect. fetchType must be "a", "o", "n" ');
        except Exception as exc: print(exc);gxc=exc; raise(gxc)
        return res
