import json

from webserver.apps.login.login_api import checkLogin

def checkPermissionAPI(func = None,role = None):
    def decorator(func):
        def decorated(self, *args, **kwargs):
            try :
                if not checkLogin(self) :
                    return self.write(json.dumps(
                        {
                            'status' : 401,
                            'message' : 'لطفا وارد شوید'
                        }
                    ))

                else :

                    if role is None :
                        return func(self, *args, **kwargs)

                    else :
                        if 'roles' in self.session :
                            userRoles = list(self.session['roles'])

                            if type(role) is str :
                                if role in userRoles :
                                    return  func(self, *args, **kwargs)

                                else :
                                    return self.write(json.dumps(
                                        {
                                            'status' : 403,
                                            'message' : 'دسترسی لازم برای این درخواست را ندارید'
                                        }
                                    ))
                            
                            elif type(role) is list :

                                if not set(role).isdisjoint(userRoles) :
                                    #check two list has sharing
                                    #https://stackoverflow.com/questions/3170055/test-if-lists-share-any-items-in-python
                                    return  func(self, *args, **kwargs)
                                
                                else :
                                    return self.write(json.dumps(
                                        {
                                            'status' : 403,
                                            'message' : 'دسترسی لازم برای این درخواست را ندارید'
                                        }
                                    ))
                            
                            else :
                                return self.write(json.dumps(
                                    {
                                        'status' : 500,
                                        'message' : 'دسترسی نا معتبر'
                                    }
                                ))

                        else :
                            return self.write(json.dumps(
                                {
                                    'status' : 500,
                                    'message' : 'نشست دسترسی یافت نشد'
                                }
                            ))
                return self.write(json.dumps(
                    {
                        'status' : 500,
                        'message' : 'نشست نقش ها یافت نشد'
                    }
                ))
                
            except Exception as error:
                print("try except")
                print("------------------------" + str(error) + "------------------------")
                return self.write(json.dumps(
                    {
                        'status' : 500,
                        'message' : 'خطای سرور'
                    }
                ))
        return decorated
    if func is None :
        return decorator
    else :
        return decorator(func)



def checkPermission(func = None,role = None):
    def decorator(func):
        def decorated(self, *args, **kwargs):
            try :
                if not checkLogin(self) :
                    return self.redirect('/')

                else :

                    if role is None :
                        return func(self, *args, **kwargs)

                    else :
                        if 'roles' in self.session :
                            userRoles = list(self.session['roles'])

                            if type(role) is str :
                                if role in userRoles :
                                    return  func(self, *args, **kwargs)

                                else :
                                    return self.write("<h1>دسترسی لازم برای این درخواست را ندارید</h1>")
                            
                            elif type(role) is list :

                                if not set(role).isdisjoint(userRoles) :
                                    #check two list has sharing
                                    #https://stackoverflow.com/questions/3170055/test-if-lists-share-any-items-in-python
                                    return  func(self, *args, **kwargs)
                                
                                else :
                                    return self.write("<h1>دسترسی لازم برای این درخواست را ندارید</h1>")
                            
                            else :
                                return self.write("<h1>دسترسی نا معتبر</h1>")

                        else :
                            return self.write("<h1>نشست دسترسی یافت نشد</h1>")
                return self.write("<h1>خطا</h1>")
                
            except Exception as error:
                print("try except")
                print("------------------------" + str(error) + "------------------------")
                return self.write("<h1>خطای سرور</h1>")
        return decorated
    if func is None :
        return decorator
    else :
        return decorator(func)