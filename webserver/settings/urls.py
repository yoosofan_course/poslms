import tornado.web
import sys, os

apps_path = os.path.join(os.path.dirname(__file__), '..', 'apps')
if  apps_path not in sys.path:
    sys.path.append(apps_path)

from webserver.apps.login import login_ui
from webserver.apps.login import login_api
from webserver.apps.logout import logout
from webserver.apps.admin import admin
from webserver.apps.teacher import teacher
from webserver.apps.register import register
from webserver.apps.ta import ta
from webserver.apps.student import student
from webserver.apps.exercise import exercise
from webserver.apps.broadcast import broadcast
from webserver.apps.course import course
from webserver.apps.lesson import lesson
from webserver.apps.professor import professor
from webserver.apps.password import changePassword
from webserver.apps.chat import chat, socket
from webserver.settings.server import MEDIA_ROOT

urls = [
    (r"/media/(.*)", tornado.web.StaticFileHandler, {"path": MEDIA_ROOT}),
    tornado.web.url(r'/', login_ui.Login, name="login_ui"),
    tornado.web.url(r'/api/login', login_api.Login, name="login_api"),
    tornado.web.url(r'/logout', logout.Logout, name="logout"),
    tornado.web.url(r'/register', register.register, name="logout"),

    ######################CHAT ##############################
    tornado.web.url(r'/chat', chat.UI, name="logout"),
    (r"/chatsocket", socket.ChatSocketHandler),


	######################‌‌Broadcast #########################
	tornado.web.url(r'/api/broadcast/add', broadcast.AddBroadcast, name="add_broadcast" ),
	tornado.web.url(r'/api/broadcast/edit', broadcast.EditBroadcast, name="edit_broadcast" ),
	tornado.web.url(r'/api/broadcast/del/([0-9]+)', broadcast.DelBroadcast, name="del_broadcast" ),
	tornado.web.url(r'/api/broadcast/showall', broadcast.ShowAll, name="show_broadcast" ),
	
    ######################exercise #########################
	tornado.web.url(r'/api/exercise/exerciseUsers/([0-9]+)', exercise.ExerciseUsers, name="usersexercise" ),
	tornado.web.url(r'/api/exercise/showCourseExercise/([0-9]+)', exercise.ShowCE, name="usersexercise" ),
    tornado.web.url(r'/api/exercise/submitScore/([0-9]+)', exercise.SubmitScore, name="usersexercise" ),
    ######################admin panel#######################
    
    ######################‌‌Broadcast #########################
    ######################exercise #########################
    tornado.web.url(r'/api/broadcast/add', broadcast.AddBroadcast, name="add_broadcast" ),
    tornado.web.url(r'/api/broadcast/edit', broadcast.EditBroadcast, name="edit_broadcast" ),
    tornado.web.url(r'/api/broadcast/del/([0-9]+)', broadcast.DelBroadcast, name="del_broadcast" ),
    tornado.web.url(r'/api/broadcast/showall', broadcast.ShowAll, name="show_broadcast" ),
    ######################exercise #########################
    tornado.web.url(r'/api/exercise/add', exercise.AddExercise, name="add_exercise" ),
    tornado.web.url(r'/api/exercise/userCourses', exercise.UserCourses, name="user_courses"),
    tornado.web.url(r'/api/course/exercises', course.GetCourseExercises, name="course_exercises"),
    tornado.web.url(r'/api/exercise/del', course.DelExercise, name="delete-exercise"),
    tornado.web.url(r'/api/exercise/studentExercises', exercise.studentCourses, name="student-exercises"),
    tornado.web.url(r'/api/exercise/courseExercise', exercise.GetCourseExercises, name="student-course-exercise"),
    tornado.web.url(r'/api/exercise_answer/add', exercise.ExerciseAnswer, name="answer-exercise"),
    tornado.web.url(r'/api/exercise_answer/marks', exercise.ExerciseAnswerMarks, name="exercises_marks"),
    ######################admin panel#######################
    tornado.web.url(r'/adminPanel', admin.Panel, name="adminPanel"),
    tornado.web.url(r'/adminPanel/htmlHandler/(\w+\.html)', admin.HtmlHandler, name="adminPanelHtmlHandler"),
    tornado.web.url(r'/api/lesson/add', lesson.AddLesson, name="add_lesson"),
    tornado.web.url(r'/api/professor/add', professor.AddProfessor, name="add_professor"),
    tornado.web.url(r'/api/password', changePassword.ChangePassword, name="change_password"),
    ######################teacher panel#######################
    tornado.web.url(r'/teacherPanel', teacher.Panel, name="teacherPanel"),
    tornado.web.url(r'/teacherPanel/htmlHandler/(\w+\.html)', teacher.HtmlHandler, name="teacherPanelHtmlHandler"),
    ######################ta panel#######################
    tornado.web.url(r'/taPanel', ta.Panel, name="taPanel"),
    tornado.web.url(r'/taPanel/htmlHandler/(\w+\.html)', ta.HtmlHandler, name="taPanelHtmlHandler"),
    ######################student panel#######################
    tornado.web.url(r'/stuPanel', student.Panel, name="stuPanel"),
    tornado.web.url(r'/stuPanel/htmlHandler/(\w+\.html)', student.HtmlHandler, name="stuPanelHtmlHandler"),
    ####################course########################
    tornado.web.url(r'/api/course/add', course.AddCourse, name="add_course"),
    tornado.web.url(r'/api/course/link_generator', course.GenerateLinkForCourse, name="add_course"),
    tornado.web.url(r'/api/course/link_revoker', course.RevokeLinkForCourse, name="add_course"),
    tornado.web.url(r'/joinCourse/(\w+)', course.JoinCourse, name="joinCourse"),
    tornado.web.url(r'/api/member/add', course.AddMember, name="add_member"),
    tornado.web.url(r'/api/member/del', course.DelMember, name="delete_member"),
    tornado.web.url(r'/api/member/getcoursemember', course.GetCourseMembers, name="get_course_members"),

]