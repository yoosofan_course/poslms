import tornado.web, os
from .urls import urls
settings = {
    "cookie_secret": "Some Random Text For Cookie Secret",
    "login_url": "/login",
    "template_path": os.path.join(os.path.dirname(__file__), '..', "templates"),
    "static_path"  : os.path.join(os.path.dirname(__file__), '..', "static"   ),
    "xsrf_cookies": True,
	############################# tornado session using redis #############################
	#https://github.com/MitchellChu/torndsession/blob/master/demos/redis_session.py
	"session": dict(
			driver="redis",
			driver_settings=dict(
				host='localhost',
				port=6379,
				db=0,
				max_connections=1024,
			),
			session_lifetime=3600
		)
	#############################  tornado session using redis #############################
}


#############################  DEBUG #############################  DEBUG
#http://stackoverflow.com/questions/12031007/disable-static-file-caching-in-tornado
class MyStaticFileHandler(tornado.web.StaticFileHandler):
    def set_extra_headers(self, path):
        # Disable cache
        self.set_header('Cache-Control', 'no-store, no-cache, must-revalidate, max-age=0')
#############################  DEBUG #############################  DEBUG

def set_app_handlers_settings():
    return urls

