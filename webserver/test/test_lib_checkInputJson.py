import unittest
import sys
import json

sys.path.append("..")
from lib.general_functions import checkInputJson

class selfSimulate:
    def __init__(self, json):
        self.request = request(json)

class request:
    def __init__(self, json):
        self.body = json

class TestStringMethods(unittest.TestCase):

    def test_parameter_check(self):
        data = {'data': {'par1':'test', 'par2':'test2'} }
        j = json.dumps(data).encode("utf-8")
        obj = selfSimulate(j)
        out = checkInputJson(obj, ["*par1", "*par2", "optional"], "data")
        self.assertEqual(out, False)

    def test_parameter_check2(self):
        data = {'data': {'par1':'test', 'par2':'test2'} }
        j = json.dumps(data).encode("utf-8")
        obj = selfSimulate(j)
        out = checkInputJson(obj, ["*par1", "*par2"], "data")
        self.assertNotEqual(out, False)


    def test_invalidparameters(self):
        data = {'data': {'par1':'test', 'par2':'test2'} }
        j = json.dumps(data).encode("utf-8")
        obj = selfSimulate(j)
        out = checkInputJson(obj, ["*par1", "*par2", "*optional"], "data")
        self.assertEqual(out, False)

    def test_optional_with_not_value(self):
        data = {'data': {'par1':'test', 'par2':'test2', 'optional':''} }
        j = json.dumps(data).encode("utf-8")
        obj = selfSimulate(j)
        out = checkInputJson(obj, ["*par1", "*par2", "optional"], "data")
        self.assertNotEqual(out, False)


if __name__ == '__main__':
    unittest.main()