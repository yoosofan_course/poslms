import json
import tornado
import tornado.web
import tornado.websocket
from webserver.apps.chat.radis_session_manager import SESSION_POOL
from webserver.apps.chat.model import ChatModel
class ChatSocketHandler(tornado.websocket.WebSocketHandler):
    waiters = set()
    cache = []
    cache_size = 200
    

    def get_compression_options(self):
        # Non-None enables compression with default options.
        return {}

    def open(self):
        print(ChatSocketHandler.waiters)
        print("here")
        self.user = None
        print(self.application.db)
        ChatSocketHandler.waiters.add(self)
        self.write_message({'type':'socket-start'})
    def on_close(self):
        print("we are closing")
        print(ChatSocketHandler.waiters)
        ChatSocketHandler.waiters.remove(self)

    @classmethod
    def update_cache(cls, chat):
        cls.cache.append(chat)
        if len(cls.cache) > cls.cache_size:
            cls.cache = cls.cache[-cls.cache_size:]

    @classmethod
    def send_updates(cls, chat):
        logging.info("sending message to %d waiters", len(cls.waiters))
        for waiter in cls.waiters:
            try:
                waiter.write_message(chat)
            except:
                logging.error("Error sending message", exc_info=True)
    async def send_message_to_online_user(sender, receiver, message, id):
        for waiter in ChatSocketHandler.waiters:
            print(waiter)
            if waiter.user == receiver:
                waiter.write_message(json.dumps({
                    'type':'receive',
                    'sender': sender,
                    'message': message,
                    'id': id,
                }))
                return 0

    async def on_message(self, message):
        parsed = tornado.escape.json_decode(message)
        print(parsed)
        if self.user == None:
            if 'token' in parsed:
                self.user = SESSION_POOL.get_user(parsed['token'])
            else:
                return self.write("un authorized")
        print(self.user)

        request = parsed['request']

        if request['type'] == 'send_message':
            sender = self.user
            receiver = request['to']
            #we can protect who can send message to whom here
            message = request['text']
            print(receiver)
            id = await ChatModel.send_message(self.application.db, sender, receiver, message)
            await ChatSocketHandler.send_message_to_online_user(sender, receiver, message, id)
            self.write_message(json.dumps({
                'type':'message_sent',
                'id':id,
                'to':receiver,
                'message':message
            }))
        elif request['type'] == 'init':
            data = await ChatModel.get_users(self.application.db, self.user)  
            print(data)          
            return self.write_message(json.dumps({'type':'init', 'data':data}))
        elif request['type'] == 'peer_messages':
            data = await ChatModel.get_peer_messages( self.application.db, self.user, request["peer"], limit = request["limit"], offset = request["offset"])
            return self.write_message(json.dumps({'type':'peer_messages', 'data':data, "peer":request["peer"],}))
        
        elif request['type'] == 'message_to_new_user':
            check = await ChatModel.check_peer_existence(self.application.db, request['user'])
            if check == True:
                return self.write_message(json.dumps({'type':'init_new_user','status':True, 'user':request['user']}))
            else:
                return self.write_message(json.dumps({'type':'init_new_user','status':False, 'user':request['user']}))