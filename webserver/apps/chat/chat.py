import json
from base.base_handler import BaseHandler
from webserver.lib.decorators import checkPermission, checkPermissionAPI
from webserver.settings.server import MEDIA_ROOT
from webserver.apps.core.file.file import File
from webserver.apps.core.file.exception import FileValidateBase
from webserver.lib.general_functions import checkInputJson
from webserver.apps.chat.radis_session_manager import SESSION_POOL
import pickle  
class UI(BaseHandler):
    @checkPermissionAPI(role=['admin','teacher','teacher_assistant','student'])
    async def get(self):
        print(self.session['username'])
        token = SESSION_POOL.new_user(self.session['username'])
        return self.render('chat/index.html', token=token)

