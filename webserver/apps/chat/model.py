class ChatModel:
    async def get_message(db, user):
        result = self.db.q("")
        print(result)
    
    async def get_users(db, current_user):
        sql = '''SELECT receiver FROM message WHERE sender = %s 
            UNION SELECT sender FROM message WHERE receiver = %s
            '''
        result = await db.q(sql , 'a', (current_user, current_user))
        return result['data']
    
    async def check_peer_existence(db, user):
        sql = ''' 
            SELECT username FROM users WHERE username = %s
        '''
        result = await db.q(sql , 'o', (user, ))
        if result['data'] == None:
            return False
        else:
            return True

    async def get_peer_messages(db, current_user, peer, limit = 10, offset = 0):
        sql = '''SELECT id,sender,receiver,text FROM message INNER JOIN text_message ON message.id = text_message.message
            WHERE (sender=%s and receiver=%s) or (sender=%s and receiver=%s)
            ORDER BY id limit %s offset %s; 
            '''
        result = await db.q(sql , 'a', (current_user, peer, peer, current_user, limit, offset))
        return result['data']
    async def send_message(db, sender, receiver, message):
        max_id = await db.q('SELECT MAX(id) from message', 'o')
        print(max_id)
        if max_id['data'][0] == None or len(max_id['data']) == 0:
            max_id = 0
        else:
            max_id = int(max_id['data'][0]) + 1
        insert_message_sql = '''
            INSERT INTO message(id, sender, receiver, senddatetime, type)
            values(%s ,%s , %s, current_timestamp, 'text')
        '''
        insert_text_sql = '''
            INSERT INTO text_message(message, text) values(%s, %s)
        '''
        transaction = await db.transaction(
            (
                (insert_message_sql, (max_id, str(sender), str(receiver))),
                (insert_text_sql, (max_id, message))
            )
        )
        print(transaction)
        return max_id