import random

## we use this method for authentication user throug websocket 
## because our session library that we using is not appropriate for 
## websocket. we might create our ownn fork of this library
class RadisSessionManager:
    def __init__(self, radis_client):
        self.users = []
    def new_user(self, username):
        string = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'
        token = ''
        for i in range(0,20):
            token += random.choice(string)
        self.users.append({'token':token, 'user':username})
        return token

    def get_user(self, token):
        for user in self.users:
            if user['token'] == token:
                return user['user']
        return None

SESSION_POOL = RadisSessionManager(None)