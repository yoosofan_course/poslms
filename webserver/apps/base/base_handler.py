from torndsession.sessionhandler import SessionBaseHandler

class BaseHandler(SessionBaseHandler):
    @property
    def db(self):
        return self.application.db