from base.base_handler import BaseHandler
from webserver.lib.decorators import checkPermission

class Panel(BaseHandler):
    @checkPermission(role="admin")
    async def get(self):
        self.render("admin/index.html",username=self.session['username'])

class HtmlHandler(BaseHandler):
    @checkPermission(role="admin")
    async def get(self,filename):
        allowedFiles = [
            "tt.html",
            "add_lesson.html",
            "add_professor.html",
            "change_password.html"
        ]

        if filename in allowedFiles :
            fileIndex = allowedFiles.index(filename)
            self.render("admin/" + allowedFiles[fileIndex])

        else :
            self.write("<h1>فایل مورد نظر شما یافت نشد</h1>")