from base.base_handler import BaseHandler

class Logout(BaseHandler):
	async def get(self):
		sessionKeys = list(self.session.keys())
		for key in sessionKeys :
			del self.session[key]
		self.clear_all_cookies()
		return self.redirect('/')