from base.base_handler import BaseHandler
from webserver.lib.decorators import checkPermission

class Panel(BaseHandler):
    @checkPermission(role="student")
    async def get(self):
        self.render("student/index.html",username=self.session['username'])

class HtmlHandler(BaseHandler):
    @checkPermission(role="student")
    async def get(self,filename):
        allowedFiles = [
            "change_password.html",
            "exercises.html",
            "marks.html",
        ]

        if filename in allowedFiles :
            fileIndex = allowedFiles.index(filename)
            self.render("student/" + allowedFiles[fileIndex])

        else :
            self.write("<h1>فایل مورد نظر شما یافت نشد</h1>")