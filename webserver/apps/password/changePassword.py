import json
import hashlib
from secrets import choice
import string

from base.base_handler import BaseHandler
from webserver.lib.decorators import checkPermissionAPI
from webserver.settings.server import MEDIA_ROOT
from webserver.lib.general_functions import checkInputJson

class ChangePassword(BaseHandler):
    @checkPermissionAPI(role=['admin','teacher','teacher_assistant','student'])
    async def post(self):
        try:
            postArgs = checkInputJson(self,["*current_password","*new_password","*repeat_new_password"])
            if postArgs is False :
                return self.write(json.dumps({'status':403, 'message':'لطفا فیلد های ضروری را وارد کنید '}))

            query = "SELECT * FROM users WHERE username=%s AND password=MD5(CONCAT(MD5(%s),(SELECT salt FROM users WHERE username=%s limit 1)));"
            queryResyalt = await self.db.q(query , 'o', (self.session['username'] ,  postArgs['current_password'], self.session['username']))
            if queryResyalt['data'] is None or not queryResyalt['flag'] :
                return self.write(json.dumps({'status' : 404 , 'message' : 'پسورد وارد شده اشتباه میباشد'}))
            
            if postArgs['new_password'] != postArgs['repeat_new_password'] :
                return self.write(json.dumps({'status': 403 ,'message':'پسوردها مطابقت ندارند'}))

            newPasswordMD5 = hashlib.md5(postArgs['new_password'].encode()).hexdigest()
            salt = ''.join([choice(string.ascii_uppercase + string.digits) for _ in range(40)])
            finalMd5 = hashlib.md5((newPasswordMD5+salt).encode()).hexdigest()

            updateQuery = "UPDATE users SET password=%s , salt=%s WHERE username=%s"
            updateQueryResault = await self.db.q(updateQuery , 'n', (finalMd5 ,  salt, self.session['username']))
            if not updateQueryResault['flag']:
                return self.write(json.dumps({'status':403 ,'message':'خطا به هنگام بروز رسانی'}))

            return self.write(json.dumps({'status' : 200 , 'message' : 'پسورد با موفقیت تغییر یافت'}))
        
        except Exception as err :
            print(err)
            return self.write(json.dumps({'status':403 ,'message':'خطا در سرور!!'}))