from base.base_handler import BaseHandler
import json
from webserver.lib.decorators import checkPermission
from webserver.settings.server import MEDIA_ROOT
from webserver.apps.core.file.file import File
from webserver.apps.core.file.exception import FileValidateBase
from webserver.lib.general_functions import checkInput
import hashlib
import string
from secrets import choice
from random import randint
from webserver.lib.general_functions import checkInput, checkInputJson
class register(BaseHandler):
    async def get(self):
        self.render("register/index.html")
    async def post(self):
        try:
            data = json.loads((self.request.body).decode("utf-8"))['data']
            newPasswordMD5 = hashlib.md5(data['password'].encode()).hexdigest()
            salt = ''.join([choice(string.ascii_uppercase + string.digits) for _ in range(40)])
            finalMd5 = hashlib.md5((newPasswordMD5+salt).encode()).hexdigest()
            check_args = checkInputJson(self, ["*username", "*email", "*telegram", "*name", "*lname"])
            if check_args is False :
                return self.write(json.dumps({'status':403, 'message':'لطفا فیلد های ضروری را وارد کنید '}))
            Query = "SELECT * FROM users  where username = %s;"
            QueryResult = await self.db.q(Query,"a",(check_args['username'],))
            print(QueryResult)
            print("checkargs", check_args)
            if QueryResult['data'] is  None  or len(QueryResult['data']) == 0 :
                Query = "INSERT INTO users (username, password, salt, email, telegram, name, last_name , confirm) VALUES (%s, %s, %s, %s, %s, %s, %s , %s)"
                Query1 = "INSERT INTO role_user (username, name) VALUES (%s, %s)"
                transaction = await self.db.transaction(
                    (
                        (Query,(check_args['username'], finalMd5 , salt, check_args['email'], check_args['telegram'], check_args['name'], check_args['lname'] , False)),
                        (Query1,(check_args['username'],'student'))
                    )
                )
                self.write(json.dumps({'status':200, 'message':'مثلا درست انجام شد'}))
            else:
                self.write(json.dumps({'status':406, 'message':'این نام کاربری تکراری است'})) 
        except Exception as err:
            print(err)
            self.write(json.dumps({'status':403, 'message':'خطا '})) 