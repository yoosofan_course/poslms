import json
import jdatetime
import hashlib
import random
import psycopg2

from base.base_handler import BaseHandler
from webserver.lib.decorators import checkPermissionAPI
from webserver.settings.server import MEDIA_ROOT
from webserver.apps.core.file.file import File
from webserver.apps.core.file.exception import FileValidateBase
from webserver.lib.general_functions import checkInputJson, checkInput

class AddCourse(BaseHandler):
    @checkPermissionAPI(role='teacher')
    async def post(self):
        try:
            postArgs = checkInputJson(self,["*course_code","*course_name","*course_description"])
            if postArgs is False :
                return self.write(json.dumps({'status':403, 'message':'لطفا فیلد های ضروری را وارد کنید '}))
            q = await self.db.q("select code from lesson where code=%s", 'o', (postArgs['course_code'],))
            if q['data'] is not None and q['flag']:
                currentTime = int(jdatetime.datetime.now().strftime("%Y%m%d"))
                e = await self.db.q("insert into course(lesson, name, description, creator, create_time, semester) \
                    values (%s, %s, %s, %s, %s, (select id from current_semester order by create_time DESC limit 1))", 'n', \
                    (postArgs['course_code'], postArgs['course_name'], postArgs['course_description'], str(self.session['username']), currentTime))  
                return self.write(json.dumps({'status': 200,'message': 'درس با موفقیت اضافه شد'}))
            else:
                return self.write(json.dumps({'status':404, 'message': 'کد درس یافت نشد'}))        
        except Exception as err:
            print(err)
            return self.write(json.dumps({'status':403, 'message':'خطا '}))  

class AddMember(BaseHandler) :
    @checkPermissionAPI(role='teacher')
    async def post(self):
        try:
            postArgs = checkInputJson(self,["*course","*ta_or_stu","*member_user"])
            if postArgs is False :
                return self.write(json.dumps({'status':403, 'message':'لطفا فیلد های ضروری را وارد کنید '}))

            query = "SELECT id FROM course WHERE creator = %s and id = %s;"
            user = str(self.session['username'])
            queryResult = await self.db.q( query, "o", (user, postArgs['course']))
            if queryResult['data'] is None or not queryResult['flag']:
                return self.write(json.dumps({'status':404, 'message': 'کد درس یافت نشد'}))
            
            queryResult = await self.db.q("SELECT username FROM users WHERE username=%s", 'o', (postArgs['member_user'],))
            if queryResult['data'] is None or not queryResult['flag']:
                return self.write(json.dumps({'status':404, 'message': 'کاربر یافت نشد'}))
            
            if postArgs['ta_or_stu'] not in ['student','teacher_assistant'] :
                return self.write(json.dumps({'status':404, 'message': 'سطح کاربری وارد شده اشتباه است'}))
            
            queryResult = await self.db.q("SELECT username FROM course_user WHERE course_id = %s AND username=%s AND role=%s ", 'o', \
                    (postArgs['course'],postArgs['member_user'],postArgs['ta_or_stu']))
            if queryResult['data'] is not None :
                return self.write(json.dumps({'status':403, 'message': 'این کاربر قبلا افزوده شده است'}))

            print(postArgs['ta_or_stu'])
            if postArgs['ta_or_stu'] == 'teacher_assistant' :
                queryResult = await self.db.q("SELECT username FROM role_user WHERE username=%s AND name='teacher_assistant' ", 'o', (postArgs['member_user'],))
                if queryResult['data'] is None :
                    queryResult = await self.db.q("INSERT INTO role_user(username,name) VALUES (%s,'teacher_assistant')", 'n', (postArgs['member_user'],))
                    if not queryResult['flag'] :
                        return self.write(json.dumps({'status':403, 'message': 'خطا در ثبت داده ها'}))
            
            queryResult = await self.db.q("INSERT INTO course_user (course_id,username,role) VALUES (%s,%s,%s)", 'n', \
                    (postArgs['course'],postArgs['member_user'],postArgs['ta_or_stu']))
            if queryResult['data'] is None :
                return self.write(json.dumps({'status':403, 'message': 'خطا در ثبت داده ها'}))
            
            return self.write(json.dumps({'status':200, 'message': 'کاربر با موفقیت به درس اضافه شد'}))

        except Exception as err:
            print(err)
            return self.write(json.dumps({'status':403, 'message':'خطا '}))  

class GetCourseMembers(BaseHandler) :
    @checkPermissionAPI(role='teacher')
    async def post(self) :
        postArgs = checkInputJson(self,["*course","*ta_or_stu"])
        if postArgs is False :
            return self.write(json.dumps({'status':403, 'message':'لطفا فیلد های ضروری را وارد کنید '}))
        
        query = "SELECT id FROM course WHERE creator = %s and id = %s;"
        user = str(self.session['username'])
        queryResult = await self.db.q( query, "o", (user, postArgs['course']))
        if queryResult['data'] is None or not queryResult['flag']:
            return self.write(json.dumps({'status':404, 'message': 'کد درس یافت نشد'}))
        
        if postArgs['ta_or_stu'] not in ['student','teacher_assistant'] :
            return self.write(json.dumps({'status':404, 'message': 'سطح کاربری وارد شده اشتباه است'}))
        
        queryResult = await self.db.q("SELECT name,last_name,username FROM users where username in (SELECT username FROM course_user WHERE course_id = %s AND role = %s)", 'a', \
                    (postArgs['course'],postArgs['ta_or_stu']))
        
        if queryResult['flag'] :
            return self.write(json.dumps({'status':200, 'message':'درست انجام شد' , 'data' : queryResult['data']}))
        
class GetCourseExercises(BaseHandler) :
    @checkPermissionAPI(role='teacher')
    async def post(self) :
        postArgs = checkInputJson(self,["*course"])
        if postArgs is False :
            return self.write(json.dumps({'status':403, 'message':'لطفا فیلد های ضروری را وارد کنید '}))
        
        query = "SELECT id FROM course WHERE creator = %s and id = %s;"
        user = str(self.session['username'])
        queryResult = await self.db.q( query, "o", (user, postArgs['course']))
        if queryResult['data'] is None or not queryResult['flag']:
            return self.write(json.dumps({'status':404, 'message': 'کد درس یافت نشد'}))
        queryResult = await self.db.q("SELECT id,title,description,file_name,start_date,end_date FROM exercise WHERE course = %s", 'a', \
                    ( str(queryResult['data'][0])) )
        
        if queryResult['flag'] :
            print(queryResult)

            return self.write(json.dumps({'status':200, 'message':'درست انجام شد' , 'data' : queryResult['data']}))
        else:
            print(queryResult)
            return self.write(json.dumps({'status':500, 'message':'لطفا فیلد های ضروری را وارد کنید '}))


class DelMember(BaseHandler) :
    @checkPermissionAPI(role='teacher')
    async def post(self):
        postArgs = checkInputJson(self,["*course","*ta_or_stu","*member_user"])
        if postArgs is False :
            return self.write(json.dumps({'status':403, 'message':'لطفا فیلد های ضروری را وارد کنید '}))
        
        query = "SELECT id FROM course WHERE creator = %s and id = %s;"
        user = str(self.session['username'])
        queryResult = await self.db.q( query, "o", (user, postArgs['course']))
        if queryResult['data'] is None or not queryResult['flag']:
            return self.write(json.dumps({'status':404, 'message': 'کد درس یافت نشد'}))
        
        if postArgs['ta_or_stu'] not in ['student','teacher_assistant'] :
            return self.write(json.dumps({'status':404, 'message': 'سطح کاربری وارد شده اشتباه است'}))
        
        queryResult = await self.db.q("SELECT username FROM course_user WHERE course_id = %s AND username=%s AND role=%s ", 'o', \
                    (postArgs['course'],postArgs['member_user'],postArgs['ta_or_stu']))
        if queryResult['data'] is None :
            return self.write(json.dumps({'status':403, 'message': 'کاربر وارد شده در این درس وجود ندارد'}))
        
        queryResult = await self.db.q("DELETE FROM course_user WHERE course_id = %s AND username=%s AND role=%s", 'n',\
            (postArgs['course'],postArgs['member_user'],postArgs['ta_or_stu']))
        if not queryResult['flag'] :
            return self.write(json.dumps({'status':403, 'message': 'خطا در حذف داده ها'}))
        
        return self.write(json.dumps({'status':200, 'message': 'با موفقیت حذف شد'}))


class DelExercise(BaseHandler) :
    @checkPermissionAPI(role='teacher')
    async def post(self):
        postArgs = checkInputJson(self,["*exercise"])
        if postArgs is False :
            return self.write(json.dumps({'status':403, 'message':'لطفا فیلد های ضروری'}))
        ##TODO : we should check premission

        queryResult = await self.db.q("DELETE FROM exercise WHERE id = %s ", 'n',(postArgs['exercise']))
        ## TODO : and also we should delete the media file
        if not queryResult['flag'] :
            return self.write(json.dumps({'status':403, 'message': 'خطا در حذف داده ها'}))
        
        return self.write(json.dumps({'status':200, 'message': 'با موفقیت حذف شد'}))

class GenerateLinkForCourse(BaseHandler):
    @checkPermissionAPI(role='teacher')
    async def post(self):
        try:
            postArgs = checkInput(self,["*id"])
            if postArgs is False :
                return self.write(json.dumps({'status':403, 'message':'لطفا فیلد های ضروری را وارد کنید '}))
            else:
                queryResult = await self.db.q("SELECT creator FROM course WHERE id = %s", 'o',(postArgs['id']))
                print(queryResult)
                if queryResult['data'][0] == str(self.session['username']):
                    queryResult = await self.db.q("DELETE FROM course_links WHERE course_id = %s", 'n',(postArgs['id']));
                    random_text = postArgs['id']
                    for i in range(0, 5):
                        random_text += random.choice('ABCDEFGHIJKLMNOPQRSTUVWXYZ123456789')
                    md5 = hashlib.md5(random_text.encode("utf-8"))
                    md5 = md5.hexdigest()
                    queryResult = await self.db.q("INSERT INTO course_links(course_id, link) values(%s, %s)", 'n',(postArgs['id'], md5 ));
                    return self.write(json.dumps({'status':200, 'link': 'https://127.0.0.1:5001/joinCourse/' + md5}))  
                else:
                    return self.write(json.dumps({'status':403, 'message':'فقط سازنده دوره میتواند برای آن لینک بسازد '}))  

        except Exception as err:
            print(err)
            return self.write(json.dumps({'status':403, 'message':'خطا '}))  

class JoinCourse(BaseHandler):
    @checkPermissionAPI(role="student")
    async def get(self, link):
        try:
            queryResult = await self.db.q("SELECT course_id FROM course_links WHERE link = %s", 'o',(link, ))
            if queryResult['data'] != None:
                id = queryResult['data'][0]
                queryResult = await self.db.q("INSERT INTO course_user (course_id,username,role) VALUES (%s,%s,%s)", 'n', \
                        (id, str(self.session['username']), 'student'))
                if queryResult['data'] is None :
                    return self.write(json.dumps({'status':403, 'message': 'شماه قبلا عضو این دوره شده بودید'}))
                self.write('خطا در عضویت')

            else:
                self.write('لینک شما نا معتبر است')
        except psycopg2.IntegrityError:
            self.write('شما قبلا عضو این دوره شده اید')
        except:
            self.write('خطای داخلی')

class RevokeLinkForCourse(BaseHandler):
    @checkPermissionAPI(role='teacher')
    async def post(self):
        try:
            postArgs = checkInput(self,["*id"])
            if postArgs is False :
                return self.write(json.dumps({'status':403, 'message':'لطفا فیلد های ضروری را وارد کنید '}))
            else:
                queryResult = await self.db.q("DELETE FROM course_links WHERE course_id = %s", 'n',(postArgs['id']));
                return self.write(json.dumps({'status':200, 'message':'تمام لینک ها منقضی شدند '}))  

        except Exception as err:
            print(err)
            return self.write(json.dumps({'status':403, 'message':'خطا '}))  
