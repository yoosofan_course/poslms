import json

from base.base_handler import BaseHandler
from webserver.lib.decorators import checkPermission
from webserver.settings.server import MEDIA_ROOT
from webserver.apps.core.file.file import File
from webserver.apps.core.file.exception import FileValidateBase
from webserver.lib.general_functions import checkInput, checkInputJson
class AddBroadcast(BaseHandler):
    @checkPermission(role=['teacher'])
    async def post(self):
        try:
            check_args = checkInputJson(self, ["*title", "*courseid", "*description"])
            if check_args is False :
                return self.write(json.dumps({'status':403, 'message':'لطفا فیلد های ضروری را وارد کنید '}))
            if check_args['title'] != '' and check_args['description'] != '':
                Query = "INSERT INTO broadcast (title, course, description,creator) VALUES (%s, %s, %s, %s)"
                user = str(self.session['username'])
                QueryResult = await self.db.q(Query,"n",(check_args['title'],int(check_args['courseid']),check_args['description'],user))
                self.write(json.dumps({'status':200, 'message':'مثلا درست انجام شد'}))
            else:
                self.write(json.dumps({'status':311, 'message':'لطفا تمام فیلد ها را کامل کنید'}))  
        except Exception as err:
            self.write(json.dumps({'status':403, 'message':'خطا '+str(err)}))   

class EditBroadcast(BaseHandler):
    @checkPermission(role=['teacher'])
    async def post(self):
        try:
            check_args = checkInputJson(self, ["*id", "*title", "*description"])
            if check_args is False :
                return self.write(json.dumps({'status':403, 'message':'لطفا فیلد های ضروری را وارد کنید '}))
            if check_args['title'] != '' and check_args['description'] != '':
                Query = " UPDATE  broadcast set title = %s , description = %s where creator = %s and id = %s ;"
                user = str(self.session['username'])
                QueryResult = await self.db.q(Query,"n",(check_args['title'],check_args['description'],user,check_args['id']))
                self.write(json.dumps({'status':200, 'message':'مثلا درست انجام شد'}))
            else:
                self.write(json.dumps({'status':311, 'message':'لطفا تمام فیلد ها را کامل کنید'}))  
        except Exception as err:
            print(err)
            self.write(json.dumps({'status':403, 'message':'خطا '+str(err)}))       

class ShowAll(BaseHandler):
    @checkPermission(role=['teacher'])
    async def get(self):
        try:
            Query = "SELECT * FROM broadcast WHERE creator=%s ;"
            user = str(self.session['username'])
            QueryResult = await self.db.q(Query,"a",(user,))
            if QueryResult['flag'] and QueryResult['data'] is not None and QueryResult['data'] != [] :
                print(QueryResult['data'])
                self.write(json.dumps({'status':200, 'message':'مثلا درست انجام شد' , 'data' : QueryResult['data']}))
            else:
                self.write(json.dumps({'status':500, 'message':'داده ای موجود نیست'}))   
        except Exception as err:
            self.write(json.dumps({'status':403, 'message':'خطا '+str(err)}))        

class DelBroadcast(BaseHandler):
    @checkPermission(role=['teacher'])
    async def get(self,id):
        try:
            Query = "DELETE FROM broadcast WHERE creator=%s and id=%s;"
            user = str(self.session['username'])
            QueryResult = await self.db.q(Query,"n",(user,id))
            if QueryResult['flag'] and QueryResult['data'] is not None and QueryResult['data'] != [] :
                print(QueryResult['data'])
                self.write(json.dumps({'status':200, 'message':'مثلا درست انجام شد' , 'data' : QueryResult['data']}))
            else:
                self.write(json.dumps({'status':500, 'message':'داده ای موجود نیست'}))   
        except Exception as err:
            self.write(json.dumps({'status':403, 'message':'خطا '+str(err)}))  