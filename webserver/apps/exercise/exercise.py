import json

from base.base_handler import BaseHandler
from webserver.lib.decorators import checkPermission
from webserver.settings.server import MEDIA_ROOT
from webserver.apps.core.file.file import File
from webserver.apps.core.file.exception import FileValidateBase
from webserver.lib.general_functions import checkInput, checkInputJson
class AddExercise(BaseHandler):
    @checkPermission(role=['teacher'])
    async def post(self):
        try:
            file = self.request.files
            print(self.request.arguments)
            check_args = checkInput(self, ["*title", "*description", "*start_time", "*end_time", "*course"])
            if check_args is False :
                return self.write(json.dumps({'status':403, 'message':'لطفا فیلد های ضروری را وارد کنید '}))
            print("checkargs", check_args)

            if 'file' in file and len(file['file']) > 0 :
                file = file['file'][0]
                #result = await self.db.q('')
                saved_file = File(file, ['jpg', 'png', 'zip', 'pdf'])
                file_name = saved_file.save_to_media_root('/exercises/')
                if check_args['end_time'] < check_args['start_time'] :
                    return self.write(json.dumps({'status':400, 'message':'بازه ی زمان شما نا معتبر است'}))
                
                # TODO   we should validate course
                Query = "INSERT INTO exercise (course, file_name, title, description, start_date, end_date, creator) VALUES (%s, %s, %s, %s, %s, %s, %s)"
                user = str(self.session['username'])
                QueryResult = await self.db.q(Query,"n",(check_args['course'], file_name , check_args['title'], check_args['description'], check_args['start_time'], check_args['end_time'], user))
                print(QueryResult)
                self.write(json.dumps({'status':200, 'message':'مثلا درست انجام شد'}))
            else:
                self.write(json.dumps({'status':312, 'message':'لطفا فایل خود را آپلود کنید'}))
        except FileValidateBase:	
            self.write(json.dumps({'status':404, 'message':'خطا در ثبت فایل'}))        
        except Exception as err:
            print(err)
            self.write(json.dumps({'status':403, 'message':'خطا '}))        

       

class ExerciseUsers(BaseHandler):
    @checkPermission(role=['teacher'])
    async def get(self,id):
        try:
            Query = "Select *  from exercise_answer where exercise = %s ; "
            user = str(self.session['username'])
            QueryResult = await self.db.q(Query,"a",(id,))
            if QueryResult['flag'] and QueryResult['data'] is not None :
                print(QueryResult['data'])
                self.write(json.dumps({'status':200, 'message':'مثلا درست انجام شد' , 'data' : QueryResult['data']}))
            else:
                self.write(json.dumps({'status':500, 'message':'داده ای موجود نیست'}))   
        except Exception as err:
            self.write(json.dumps({'status':403, 'message':'خطا '+str(err)}))        

class ShowCE(BaseHandler):
    @checkPermission(role=['teacher'])
    async def get(self,id):
        try:
            Query = "Select * From exercise where course = %s ;"
            user = str(self.session['username'])
            QueryResult = await self.db.q(Query,"a",(id,))
            if QueryResult['flag'] and QueryResult['data'] is not None :
                print(QueryResult['data'])
                self.write(json.dumps({'status':200, 'message':'مثلا درست انجام شد' , 'data' : QueryResult['data']}))
            else:
                self.write(json.dumps({'status':500, 'message':'داده ای موجود نیست'}))   
        except Exception as err:
            self.write(json.dumps({'status':403, 'message':'خطا '+str(err)}))        

class UserCourses(BaseHandler):
    @checkPermission(role=['teacher'])
    async def get(self):
        try:
            
            Query = "SELECT * FROM course LEFT JOIN course_user ON course_id = id where \
            ((creator	=%s) or (username = %s and role = 'teacher_assistant' )) and semester = (select id from current_semester order by create_time DESC limit 1); "
            user = str(self.session['username'])
            QueryResult = await self.db.q(Query,"a",(user,user,))
            if QueryResult['flag'] and QueryResult['data'] is not None :
                
                self.write(json.dumps({'status':200, 'message':'مثلا درست انجام شد' , 'data' : QueryResult['data']}))
            else:
                self.write(json.dumps({'status':500, 'message':'داده ای موجود نیست'}))   
        except Exception as err:
            self.write(json.dumps({'status':403, 'message':'خطا '+str(err)}))     
class studentCourses(BaseHandler):
    @checkPermission(role=['student'])
    async def get(self):
        try:
            Query = " SELECT id, name, description FROM course_user INNER JOIN course on course_user.course_id = course.id where username = %s"
            user = str(self.session['username'])
            QueryResult = await self.db.q(Query,"a",(user,))
            if QueryResult['flag'] and QueryResult['data'] is not None :
                self.write(json.dumps({'status':200, 'message':'مثلا درست انجام شد' , 'data' : QueryResult['data']}))
            else:
                self.write(json.dumps({'status':500, 'message':'داده ای موجود نیست'}))   

        except Exception as err:
            self.write(json.dumps({'status':403, 'message':'خطا '+str(err)}))        

class studentCourses(BaseHandler):
    @checkPermission(role=['student'])
    async def get(self):
        try:
            Query = " SELECT id, name, description FROM course_user INNER JOIN course on course_user.course_id = course.id where username = %s"
            user = str(self.session['username'])
            QueryResult = await self.db.q(Query,"a",(user,))
            if QueryResult['flag'] and QueryResult['data'] is not None :
                self.write(json.dumps({'status':200, 'message':'مثلا درست انجام شد' , 'data' : QueryResult['data']}))
            else:
                self.write(json.dumps({'status':500, 'message':'داده ای موجود نیست'}))   

        except Exception as err:
            self.write(json.dumps({'status':403, 'message':'خطا '+str(err)}))        

class GetCourseExercises(BaseHandler) :
    @checkPermission(role='student')
    async def post(self) :
        ##we should check that weather this course is really owned by this usre or not
        postArgs = checkInputJson(self,["*course"])
        if postArgs is False :
            return self.write(json.dumps({'status':403, 'message':'لطفا فیلد های ضروری را وارد کنید '}))
        
        queryResult = await self.db.q("SELECT id,title,description,file_name,start_date,end_date FROM exercise WHERE course = %s", 'a', \
                    ( str(postArgs['course'])) )
        
        if queryResult['flag'] :
            print(queryResult)
            return self.write(json.dumps({'status':200, 'message':'درست انجام شد' , 'data' : queryResult['data']}))
        else:
            print(queryResult)
            self.write(json.dumps({'status':403, 'message':'خطا '+str(err)}))        


class ExerciseAnswer(BaseHandler):
    @checkPermission(role='student')
    async def post(self):
        file = self.request.files
        postArgs = checkInput(self,["*id", "title", "description"])
        if postArgs is False :
            return self.write(json.dumps({'status':403, 'message':'لطفا فیلد های ضروری را وارد کنید '}))
        if 'file' in file and len(file['file']) > 0 :
            file = file['file'][0]
            saved_file = File(file, ['jpg', 'png', 'zip', 'pdf'])
            file_name = saved_file.save_to_media_root('/exercises_answers/')
            user = str(self.session['username'])
            id = postArgs['id']
            Query = "SELECT * FROM exercise_answer WHERE exercise = %s and username = %s "
            print(id, user)
            QueryResult = await self.db.q(Query,"a",(id, user))
            print(Query, QueryResult)
            if QueryResult['flag'] and QueryResult['data'] is not None  and len(QueryResult['data']) > 0 :
                self.write(json.dumps({'status':200, 'message':'این تمرین قبلا ثبت شده است' , 'data' : QueryResult['data']}))
            else:
                # TODO   we should check weather this exercises is in student course
                Query = "INSERT INTO exercise_answer (exercise, username, title, description, file_name) VALUES (%s, %s, %s, %s, %s)"
                QueryResult = await self.db.q(Query,"n", (id, user, postArgs['title'], postArgs['description'], file_name ))
                print(QueryResult)
                self.write(json.dumps({'status':200, 'message':'مثلا درست انجام شد'}))
        else:
            self.write(json.dumps({'status':312, 'message':'لطفا فایل خود را آپلود کنید'}))

class ExerciseAnswerMarks(BaseHandler):
    @checkPermission(role='student')
    async def post(self):
        postArgs = checkInputJson(self,["*course"])
        if postArgs == False:
            return self.write(json.dumps({'status':403, 'message':'لطفا فیلد های ضروری را وارد کنید '}))
        
        id = postArgs['course']
        user = str(self.session['username'])
        Query = "SELECT * FROM exercise_answer INNER JOIN exercise on exercise_answer.exercise = exercise.id  WHERE course = %s and username = %s "
        print(id, user)
        QueryResult = await self.db.q(Query,"a",(id, user))
        print(Query, QueryResult)
        if QueryResult['flag'] and QueryResult['data'] is not None  and len(QueryResult['data']) > 0 :
            self.write(json.dumps({'status':200, 'data' : QueryResult['data']}))
        else:
            self.write(json.dumps({'status':200, 'data' : []}))

class SubmitScore(BaseHandler):
    @checkPermission(role=['teacher'])
    async def post(self,id):
        try:
            Query = "SELECT * FROM exercise inner join course on exercise.course = course.id where course.creator = %s "
            user = str(self.session['username'])
            QueryResult = await self.db.q(Query,"a",(user,))
            if QueryResult['flag'] and QueryResult['data'] is not None  and len(QueryResult['data']) > 0 :
                biggerthan = False
                for i in self.request.arguments:
                    if(int(self.request.arguments[i][0].decode("utf-8", "strict"))<101):
                        Query = " UPDATE exercise_answer SET grade=%s where username = %s;"
                        QueryResult = await self.db.q(Query,"n",(self.request.arguments[i][0].decode("utf-8", "strict"),i))
                    else:
                        biggerthan = True
                if biggerthan == True :
                    self.write(json.dumps({'status':200, 'message':'برخی نمره ها بالای 100 بودند و ثبت نشدند بقیه ثبت شده اند'}))
                self.write(json.dumps({'status':200, 'message':'مثلا درست انجام شد'}))
            else:
                self.write(json.dumps({'status':403, 'message':'شما به این درس دسترسی ندارید' , 'data' : QueryResult['data']}))
        except Exception as err:
            self.write(json.dumps({'status':403, 'message':'خطا '+str(err)}))   