from .exception import InvalidContentType, UndefinedFormat
from webserver.settings.server import MEDIA_ROOT
from datetime import timezone, datetime
class File:
    def __init__(self, file , ALLOWED = ['jpg', 'zip', 'pdf', 'png']):
        self.file = file
        self.ALLOWED = ALLOWED
        self.filename = ''
        self.extension = ''
        self.validateFile()


    def getFormat(filename):
        splits = filename.split(".")
        if len(splits) == 0:
            raise UndefinedFormat
        return splits[len(splits) - 1]
    

    def validateFile(self):
        # we should also check the size
        formats = {
            'rst':{'content-type':['text/x-rst']},
            'jpg':{'content-type':''},
            'png':{'content-type':'image/png'},
            'zip':{'content-type':''},
        }
        file_format = File.getFormat(self.file['filename'])
        self.extension = file_format
        if file_format in self.ALLOWED:
            print(self.file['content_type'])
            print(formats[file_format])
            if self.file['content_type'] != formats[file_format]['content-type']:
                raise InvalidContentType
            else:
                filename = self.file['filename']
                filename = filename[0:len(filename) - 1 - len(file_format)]
                self.filename = filename
        else:
            raise UndefinedFormat
    def get_name(self):
        return self.filename.strip().replace(" ","") + '.' + self.extension

    def save_to_media_root(self, path):
        random = str(round(datetime.now(tz=timezone.utc).timestamp() * 1000))
        output_file = open(MEDIA_ROOT + path + random + '.' + self.extension , 'wb')
        output_file.write(self.file['body'])
        return random + '.' + self.extension
