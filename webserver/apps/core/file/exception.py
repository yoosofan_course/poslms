class FileValidateBase(Exception):
    pass

class InvalidFormat(FileValidateBase):
    pass

class UndefinedFormat(FileValidateBase):
    pass

class InvalidContentType(FileValidateBase):
    pass