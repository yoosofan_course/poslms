class ValidateBase(Exception):
    pass

class InvalidFormat(ValidateBase):
    pass

class UndefinedFormat(ValidateBase):
    pass

class InvalidContentType(ValidateBase):
    pass