import json

from base.base_handler import BaseHandler
from webserver.lib.decorators import checkPermissionAPI
from webserver.settings.server import MEDIA_ROOT
from webserver.lib.general_functions import checkInputJson


class AddLesson(BaseHandler):
    @checkPermissionAPI(role=['admin'])
    async def post(self):
        try:
            postArgs = checkInputJson(self,["*lesson_code","*lesson_title"])

            if postArgs != False:
                Query = "SELECT code FROM lesson WHERE code = %s"
                QueryResult = await self.db.q(Query , "o" , (postArgs['lesson_code'], ))

                if QueryResult['data'] != None and QueryResult['flag']:
                    self.write(json.dumps({'status':311, 'message':'کددرس تکراری ست'}))

                else:
                    Query = "INSERT INTO lesson (code, name) VALUES (%s, %s)"
                    QueryResult = await self.db.q(Query,"n",(postArgs['lesson_code'],postArgs['lesson_title']))
                    if QueryResult['flag']:
                        self.write(json.dumps({'status':200, 'message':'درس با موفقیت اضافه شد'}))
            else:
                self.write(json.dumps({'status':311, 'message':'لطفا تمام فیلد ها را کامل کنید'}))
        except FileValidateBase:
            self.write(json.dumps({'status':404, 'message':'خطا در ثبت فایل'}))        
        except Exception as err:
            self.write(json.dumps({'status':403, 'message':'خطا '}))        
