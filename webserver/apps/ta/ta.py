from base.base_handler import BaseHandler
from webserver.lib.decorators import checkPermission

class Panel(BaseHandler):
    @checkPermission(role="teacher_assistant")
    async def get(self):
        self.render("ta/index.html",username=self.session['username'])

class HtmlHandler(BaseHandler):
    @checkPermission(role="teacher_assistant")
    async def get(self,filename):
        allowedFiles = [
            "change_password.html"
        ]

        if filename in allowedFiles :
            fileIndex = allowedFiles.index(filename)
            self.render("ta/" + allowedFiles[fileIndex])

        else :
            self.write("<h1>فایل مورد نظر شما یافت نشد</h1>")