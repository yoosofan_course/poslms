from base.base_handler import BaseHandler
from webserver.lib.decorators import checkPermission

class Panel(BaseHandler):
    @checkPermission(role="teacher")
    async def get(self):
        self.render("teacher/index.html",username=self.session['username'])

class HtmlHandler(BaseHandler):
    @checkPermission(role="teacher")
    async def get(self,filename):
        allowedFiles = [
            'add_exercise.html',
            'manage_exercise.html',
            'broadcast.html',
            'broadcastform.html',
			'add_course.html',
            "change_password.html",
			'add_member.html',
            'score.html',
			'delete_member.html',
            'manage_courses.html',
        ]

        if filename in allowedFiles :
            fileIndex = allowedFiles.index(filename)
            self.render("teacher/" + allowedFiles[fileIndex])


        else :
            self.write("<h1>فایل مورد نظر شما یافت نشد</h1>")