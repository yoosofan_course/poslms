import json

from base.base_handler import BaseHandler
from webserver.lib.decorators import checkPermissionAPI
from webserver.settings.server import MEDIA_ROOT
from webserver.lib.general_functions import checkInputJson
from webserver.apps.core.file.file import File
from webserver.apps.core.file.exception import FileValidateBase


class AddProfessor(BaseHandler):
    @checkPermissionAPI(role=['admin'])
    async def post(self):
        try:
            postArgs = checkInputJson(self,["*professor_user_name"])
            if postArgs != False:
                Query = "SELECT username FROM users WHERE username = %s"
                QueryResult = await self.db.q(Query , "o" , (postArgs['professor_user_name'],))
                if QueryResult['data'] != None and QueryResult['flag']:
                    QueryRole = " SELECT * FROM role_user WHERE username = %s AND name = 'teacher' "
                    QueryResultRole = await self.db.q(QueryRole , "o" , (postArgs['professor_user_name'],))

                    if QueryResultRole['data'] != None and QueryResult['flag']:
                        self.write(json.dumps({'status':201, 'message':'این کاربر قبلا استاد شده'}))
                    else:
                        Query = "INSERT INTO role_user (username,name) VALUES (%s,%s)"
                        QueryResult = await self.db.q(Query , "n" , (postArgs['professor_user_name'], "teacher"))
                        self.write(json.dumps({'status':200, 'message':'با موفقیت انجام شد'}))
                else:
                    self.write(json.dumps({'status':404, 'message':'این کاربر ثبت نشده است'}))
            else:
                self.write(json.dumps({'status':311, 'message':'لطفا تمام فیلد ها را کامل کنید'}))
        except FileValidateBase:
            self.write(json.dumps({'status':404, 'message':'خطا در ثبت فایل'}))        
        except Exception as err:
            self.write(json.dumps({'status':403, 'message':'خطا '}))        


        except FileValidateBase:
            self.write(json.dumps({'status':404, 'message':'خطا در ثبت فایل'}))        
        except Exception as err:
            self.write(json.dumps({'status':403, 'message':'خطا '}))        
