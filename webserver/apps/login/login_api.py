from base.base_handler import BaseHandler
import json

class Login(BaseHandler):
	async def post(self):
		if not checkLogin(self) :
			print(self.request.body)
			data = json.loads((self.request.body).decode("utf-8"))['data']
			username = data['username']
			password = data['password']
			loginQuery = "SELECT * FROM users WHERE username=%s AND password=MD5(CONCAT(MD5(%s),(SELECT salt FROM users WHERE username=%s limit 1)));"
			loginQueryResult = await self.db.q(loginQuery,"o",(username,password,username))
			if loginQueryResult['flag'] and loginQueryResult['data'] is not None :
				roleUserQuery = "SELECT name FROM role_user WHERE username=%s"
				roleUserResult = await self.db.q(roleUserQuery,"a",(username,))
				if roleUserResult['flag'] and len(roleUserResult['data']) > 0 :
					self.session['is_login'] = True
					self.session['username'] = username
					roles = [i[0] for i in roleUserResult['data']]
					self.session['roles'] = roles
					returnDic = {
						'status' : 301,
						'message' : 'با موفقیت وارد شدید'
					}
					if len(roles) == 1 :
						returnDic['data'] = {
							'redirect' : panelAddresses[roles[0]]['url']
						}
					else :
						returnDic['data'] = {
							'redirect' : '/'
						}
					return self.write(json.dumps(returnDic))
				else :
					return self.write(json.dumps(
						{
							'status' : 500,
							'message' : 'یوزر شما هیچ دسترسی ندارد'
						}
					))
			else :
				return self.write(json.dumps(
					{
						'status' : 401,
						'message' : 'یوزر یا پسورد اشتباه است'
					}
				))
		else :
			return self.write(json.dumps(
				{
					'status' : 400,
					'message' : 'پیشتر وارد شده اید'
				}
			))

def checkLogin(self) :
	if 'is_login' not in self.session :
		return False
	else :
		if self.session['is_login'] :
			return True
		else :
			return False

panelAddresses = {
	'admin' : {
		'url' : '/adminPanel',
		'fa' : 'پنل مدیریت'
	},
	'teacher' : {
		'url' : '/teacherPanel',
		'fa' : 'پنل اساتید'
	},
	'teacher_assistant' : {
		'url' : '/taPanel',
		'fa' : 'پنل حل تمرین'
	},
	'student' : {
		'url' : '/stuPanel',
		'fa' : 'پنل دانشجویان'
	}
}
