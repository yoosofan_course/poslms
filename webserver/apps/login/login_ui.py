from base.base_handler import BaseHandler
from .login_api import checkLogin,panelAddresses

class Login(BaseHandler):
    async def get(self):
        if not checkLogin(self) :
            self.render("login/index.html")
        else :
            roles = self.session['roles']
            if len(roles) == 1 :
                self.redirect(panelAddresses[roles[0]]['url'])
            else :
                linkes = ""
                for role in roles :
                    linkes += '<a href="' + panelAddresses[role]['url'] + '">' + panelAddresses[role]['fa'] + '</a><br>'
                return self.write(linkes)