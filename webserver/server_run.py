import tornado.httpserver, tornado.ioloop, tornado.options, tornado.web, tornado.options
import sys,os; 
import ssl
from webserver.lib import db
import webserver.settings as settings


pathDbq=os.path.join(os.path.dirname(__file__),'..','db')
print(pathDbq)
if pathDbq not in sys.path: 
    sys.path.append(pathDbq);



import dbSetting





def make_app():
    print(settings.set_app_handlers_settings())
    return tornado.web.Application(settings.set_app_handlers_settings(),**settings.settings)


def main(myDebug=True):
    tornado.options.parse_command_line()
    app = make_app()
    if myDebug:
        app.debug = True
        app.autoreload=True
        #static_hash_cache=False
    else:
        app.debug = False
        app.autoreload=False
  
    ioloop  =  tornado.ioloop.IOLoop.current()


    app.db = db.db(app,ioloop,dbSetting)
    tornado.options.define("port", default=dbSetting.webServerSetting['port'], help="run on the given port", type=int)
    ssl_ctx = ssl.create_default_context(ssl.Purpose.CLIENT_AUTH)
    SSL_DIR = os.path.dirname(os.path.abspath(__file__)) + "/settings"
    ssl_ctx.load_cert_chain(os.path.join(SSL_DIR, "kashanunilms.cert"),
                        os.path.join(SSL_DIR, "kashanunilms.key"))
    http_server = tornado.httpserver.HTTPServer(app, ssl_options = ssl_ctx)
    http_server.listen(tornado.options.options.port)
    ioloop.start()

'''
if __name__ == "__main__":
    main(myDebug=dbSetting.webServerSetting['myDebug'])
'''
