#!/bin/bash
echo "Please enter postgres password : "
read dbpassword
export PGPASSWORD=$dbpassword
psql -h 127.0.0.1 -U postgres  -f ../db/CREATE_USER_AND_DATABASE.sql
psql -h 127.0.0.1 -U postgres  -f ../db/CREATE_TABLE.sql
psql -h 127.0.0.1 -U postgres  -f ../db/INSERT_DATA.sql