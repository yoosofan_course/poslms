\c lmsproject;
CREATE TABLE "users"
(
	"username"	VARCHAR(50) NOT NULL,
	"password"	VARCHAR(50) NOT NULL,
	"salt"	VARCHAR(50) NOT NULL,
	"email"	VARCHAR(100),
	"telegram"	VARCHAR(50),
	"name"	VARCHAR(30),
	"last_name"	VARCHAR(30),
	"mobile"	VARCHAR(20),
	"confirm" BOOLEAN NOT NULL,
	PRIMARY KEY("username")
);
--====================================
CREATE TABLE "role"
(
	"name"	VARCHAR(50) NOT NULL,
	PRIMARY KEY("name")
);
--====================================
CREATE TABLE "role_user"
(
	"name" VARCHAR(50) REFERENCES "role" ("name") ON DELETE CASCADE ON UPDATE CASCADE NOT NULL,
	"username" VARCHAR(50) REFERENCES "users" ("username") ON DELETE CASCADE ON UPDATE CASCADE NOT NULL,
 	PRIMARY KEY ("name","username")
);
--====================================
CREATE TABLE "lesson"
(
    "code" VARCHAR(50) NOT NULL,
    "name" VARCHAR(200) NOT NULL,
    PRIMARY KEY ("code")
);
--====================================
CREATE TABLE "current_semester"
(
	id SERIAL NOT NULL,
	"semester" VARCHAR(1) NOT NULL,
	"year" VARCHAR(9) NOT NULL,
	"create_time" DATE NOT NULL DEFAULT CURRENT_DATE,
	UNIQUE ("semester", "year"),
	PRIMARY KEY("id")
);
--====================================
CREATE TABLE "course"
(
    "id" SERIAL NOT NULL, 
    "lesson" VARCHAR(50) REFERENCES "lesson" ("code") ON DELETE CASCADE ON UPDATE CASCADE NOT NULL,
    "name" VARCHAR(200),
    "description" VARCHAR(3000),
    "creator" VARCHAR(50) REFERENCES "users" ("username") ON DELETE CASCADE ON UPDATE CASCADE NOT NULL,
    "semester" INTEGER REFERENCES "current_semester" ("id") ON DELETE CASCADE ON UPDATE CASCADE NOT NULL,
    "create_time" INTEGER,
    PRIMARY KEY("id")
);
--====================================
CREATE TABLE "course_links"
(
	"course_id" INTEGER REFERENCES "course" ("id") ON DELETE CASCADE ON UPDATE CASCADE NOT NULL,
	"link" VARCHAR(100) NOT NULL,
	PRIMARY KEY("course_id", "link")
);
--====================================
CREATE TABLE "course_user"
(
	"course_id" INTEGER REFERENCES "course" ("id") ON DELETE CASCADE ON UPDATE CASCADE NOT NULL,
	"username" VARCHAR(50) REFERENCES "users" ("username") ON DELETE CASCADE ON UPDATE CASCADE NOT NULL,
	"role" VARCHAR(50) REFERENCES "role" ("name") ON DELETE CASCADE ON UPDATE CASCADE NOT NULL,
    PRIMARY KEY("course_id", "username", "role")
);
--====================================
CREATE TABLE "exercise"
(
	"id" SERIAL NOT NULL, 
    "course" INTEGER REFERENCES "course" ("id") ON DELETE CASCADE ON UPDATE CASCADE NOT NULL,
	"file_name" VARCHAR(100) NOT NULL,
	"title" VARCHAR(200) NOT NULL,
	"description" VARCHAR(2000),
	"start_date" INTEGER,
	"end_date" INTEGER,
	"creator" VARCHAR(50) REFERENCES "users" ("username") ON DELETE CASCADE ON UPDATE CASCADE NOT NULL,
	"size_exceed" INTEGER,
	PRIMARY KEY("id")
);
--====================================
CREATE TABLE "exercise_answer"
(
	"id" SERIAL NOT NULL, 
	"exercise" INTEGER REFERENCES "exercise" ("id") ON DELETE CASCADE ON UPDATE CASCADE NOT NULL,
	"username" VARCHAR(50) REFERENCES "users" ("username") ON DELETE CASCADE ON UPDATE CASCADE NOT NULL,
	"title" VARCHAR(300) ,
	"description" VARCHAR(1000),
	"file_name" VARCHAR(100) NOT NULL,
	"grade" INTEGER DEFAULT 0,
	PRIMARY KEY("id")
);
--====================================
CREATE TABLE "broadcast"
(
	"id" SERIAL NOT NULL, 
	"title" VARCHAR(200),
	"description" VARCHAR(2000),
	"creator" VARCHAR(50) REFERENCES "users" ("username") ON DELETE CASCADE ON UPDATE CASCADE NOT NULL,
	"course" INTEGER REFERENCES "course" ("id") ON DELETE CASCADE ON UPDATE CASCADE NOT NULL,
	PRIMARY KEY("id")
);
--====================================
CREATE TABLE "message"
(
	"id" SERIAL NOT NULL, 
	"reply_id" INTEGER REFERENCES "message" ("id") ON DELETE CASCADE ON UPDATE CASCADE NULL,
	"sender" VARCHAR(50) REFERENCES "users" ("username") ON DELETE CASCADE ON UPDATE CASCADE NOT NULL,
	"receiver" VARCHAR(50) REFERENCES "users" ("username") ON DELETE CASCADE ON UPDATE CASCADE NOT NULL,
	"order" INTEGER DEFAULT 0,
	"senddatetime" TIMESTAMP,
	"seendatatime" TIMESTAMP,
	"seen" BOOLEAN DEFAULT false,
	"type" VARCHAR(10),
	PRIMARY KEY("id")
);
--====================================
CREATE TABLE "media_message"
(
	"message" BIGINT REFERENCES "message"("id") ON DELETE CASCADE ON UPDATE CASCADE NOT NULL,
	"media" VARCHAR(100),
	"media_type" VARCHAR(20),
	PRIMARY KEY("message")
);
--====================================
CREATE TABLE "text_message"
(
	"message" BIGINT REFERENCES "message"("id") ON DELETE CASCADE ON UPDATE CASCADE NOT NULL,
	"text" VARCHAR(5000),
	PRIMARY KEY("message")
);

