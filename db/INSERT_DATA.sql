\c lmsproject;
INSERT INTO role values ('admin'),('teacher'),('teacher_assistant'),('student');
INSERT INTO users (username, password, salt, confirm) VALUES ('navid', '53794162c7681eb451eb4339777e323c', '1', True);
INSERT INTO users (username, password, salt, confirm) VALUES ('admin', '53794162c7681eb451eb4339777e323c', '1', True);
INSERT INTO users (username, password, salt, confirm) VALUES ('gholi', '53794162c7681eb451eb4339777e323c', '1', True);
INSERT INTO users (username, password, salt, confirm) VALUES ('taghi', '53794162c7681eb451eb4339777e323c', '1', True);
INSERT INTO users (username, password, salt, confirm) VALUES ('hasan', '53794162c7681eb451eb4339777e323c', '1', True);
insert into role_user(name,username) values ('teacher','taghi'),('teacher_assistant','hasan'),('teacher_assistant','navid'),('student','navid'),('admin','admin'),('teacher_assistant','gholi');
INSERT INTO lesson (code, name) VALUES ('1122', 'compiler');
INSERT INTO lesson (code, name) VALUES ('1123', 'mohandesi net');
INSERT INTO lesson (code, name) VALUES ('1124', 'os');
INSERT INTO lesson (code, name) VALUES ('1125', 'gosaste');

INSERT INTO current_semester (semester, year) VALUES ('2','1396-1397');