import pathlib,sys,os
dbSetting={}
dbSetting['host']="127.0.0.1"
dbSetting['dbname']="lmsproject"
dbSetting['user']="lmsproject"
dbSetting['password']="lM$proJ3ct"
dbSetting['port']="5432"

momokoSetting={}
momokoSetting['size']=10
momokoSetting['max_size']=18
momokoSetting['setsession']=("SET TIME ZONE UTC",)

webServerSetting={}
webServerSetting['port']=5001
webServerSetting['myDebug']=True

serverDbSetting={}
serverDbSetting['VACUUM_FULL_HOUR']='04'
serverDbSetting['VACUUM_FULL_MINUTE']='45'



debugOutputFileName=pathlib.Path(os.path.abspath(__file__)).parent.parent.parent
debugOutputFileName=str(pathlib.Path(debugOutputFileName).resolve())
debugOutputFileName+='/dailyExcelFilesProcessDebugOutput.txt'

debugProcessOssFileName=pathlib.Path(os.path.abspath(__file__)).parent.parent.parent
debugProcessOssFileName=str(pathlib.Path(debugProcessOssFileName).resolve())
debugProcessOssFileName+='/debugProcessOssFileNameOutput.txt'
