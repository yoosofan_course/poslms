CREATE USER lmsproject with encrypted password 'lM$proJ3ct';
DROP DATABASE lmsproject;
CREATE DATABASE lmsproject;
grant all privileges on database lmsproject to lmsproject ;
ALTER USER lmsproject WITH SUPERUSER;
