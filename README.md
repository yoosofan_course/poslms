# Pythonic Open Source Learning Management System (poslms)

# What is poslms?
a pythonic open source lms system that helps teachers, teacher assistants and students to communicate and learn out side of the class.


* **Database**

    * postgresql

* **Programming Language**

   * python3

* **Python Libraries**

    * tornado
    * psycopg2
    * redis
    * torndsession
    * Momoko
    * jdatetime

 
To run locally, in ubuntu ::

    cd install
    sudo bash install.sh
    cd ..
    sh run.sh

you can log in with the folowing users:
 
* navid
* gholi
* taghi
* hasan
* admiin

and password is : 123
    

## Developers 
* Omid Mohammadi
* Seyed Javad Hosseini
* Navid Mirmoghtadaie 
* Azim Atefi
* Ali Hosseinpoor
* Moien Esmaieli (UI)
* Reza Mazaheri  (UI)


## Contribute
for contribution you can check the code and there are a bunch of TODOs as comment that you can check them. and also cleaning up the code and making the structure tidier is helpful.

